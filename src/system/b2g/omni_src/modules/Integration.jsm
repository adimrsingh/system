"use strict";this.EXPORTED_SYMBOLS=["Integration",];const{classes:Cc,interfaces:Ci,utils:Cu,results:Cr}=Components;Cu.import("resource://gre/modules/XPCOMUtils.jsm");const gIntegrationPoints=new Map();this.Integration=new Proxy({},{get(target,name){let integrationPoint=gIntegrationPoints.get(name);if(!integrationPoint){integrationPoint=new IntegrationPoint();gIntegrationPoints.set(name,integrationPoint);}
return integrationPoint;},});this.IntegrationPoint=function(){this._overrideFns=new Set();this._combined={QueryInterface:function(){let ex=new Components.Exception("Integration objects should not be used with XPCOM because"+" they change when new overrides are registered.",Cr.NS_ERROR_NO_INTERFACE);Cu.reportError(ex);throw ex;},};}
this.IntegrationPoint.prototype={_overrideFns:null,_combined:null,_combinedIsCurrent:false,register(overrideFn){this._overrideFns.add(overrideFn);this._combinedIsCurrent=false;},unregister(overrideFn){this._overrideFns.delete(overrideFn);this._combinedIsCurrent=false;},getCombined(root){if(this._combinedIsCurrent){return this._combined;}


let overrideFnArray=[...this._overrideFns,()=>this._combined];let combined=root;for(let overrideFn of overrideFnArray){try{
let override=overrideFn.call(null,combined);

let descriptors={};for(let name of Object.getOwnPropertyNames(override)){descriptors[name]=Object.getOwnPropertyDescriptor(override,name);}
combined=Object.create(combined,descriptors);}catch(ex){Cu.reportError(ex);}}
this._combinedIsCurrent=true;return this._combined=combined;},defineModuleGetter(targetObject,name,moduleUrl,symbol){let moduleHolder={};XPCOMUtils.defineLazyModuleGetter(moduleHolder,name,moduleUrl,symbol);Object.defineProperty(targetObject,name,{get:()=>this.getCombined(moduleHolder[name]),configurable:true,enumerable:true,});},};