'use strict';
/* jshint nonew: false */

(function(exports) {

  function PbapPhonebook() {
    this._ctsMan = navigator.mozContacts;
    this._version;
    this._isPhoneBook;
    this._phonepbCache = [];
    this._phoneicCache = [];
    this._phoneocCache = [];
    this._phonemcCache = [];
    this._phoneccCache = [];
    this._simCache = [];
    this._path;
    this._maxListCount;
  }


  PbapPhonebook.prototype = {

    pullPhoneBook: function(evt) {
      var propSel = evt.propSelector;
      var vcardSel = evt.vcardSelector;
      var vcardOp = evt.vcardSelectorOperator;
      var vcardVersion = evt.format;
      this._version = vcardVersion;

      this.updatePath(evt.name);
      this.updateMaxCount(evt.maxListCount);
      return this.getContactsFromPath().then((contacts) => {
        contacts = window.filterByCardSelector(vcardSel, vcardOp, contacts);
        window.filterByPropSelector(vcardVersion, propSel, contacts);
        return contacts;
      });
    },

    pullVcardEntry: function(evt) {
      var path = evt.name.replace(/\.vcf$/i, '');
      var handleId = path[(path.length-1)];
      var vcardVersion = evt.format;
      var propSel = evt.propSelector;

      this.updatePath(evt.name);

      return new Promise((resolve) => {
        if (this._path === 'pb') {
          var ctsId = this._phonepbCache[handleId].ctsId;
          var filter = {
            filterBy: ['id'],
            filterValue: ctsId,
            filterOp: 'equals'
          };
          var request = this._ctsMan.find(filter);
          request.onsuccess = function() {
            resolve(this.result);
          };
          request.onerror = function(error) {
            resolve([]);
          };
        } else {
          var entry = this.getEntry(handleId);
          resolve([entry.contact]);
        }
      }).then((contacts) => {
        window.filterByPropSelector(vcardVersion, propSel, contacts);
        return contacts;
      });
    },

    pullVcardListing: function(evt) {
      var vcardSel = evt.vcardSelector;
      var vcardOp = evt.vcardSelectorOperator;

      this.updatePath(evt.name);
      this.updateMaxCount(evt.maxListCount);

      return this.getContactsFromPath().then((contacts) => {
        contacts = window.filterByCardSelector(vcardSel, vcardOp, contacts);
        return contacts;
        }).then((contacts) => {
          return this.generateListingCache(contacts);
        }).then((cacheList) => {
          return this.generateVcardList(cacheList);
        });
    },

    generateListingCache: function(contacts) {
      var cache = [];
      var self = this;

      this.cleanCache();

      contacts.forEach((item) => {
        var cacheItem = {};
        if (self._path === 'pb') {
          cacheItem.ctsId = item.id;
        } else {
          cacheItem.contact = item;
        }
        if (self._path !== 'pb' && self._path !== 'sim') {
          cacheItem.name = item.name[0] +';';
        } else {
          cacheItem.name = item.familyName + ';' + item.givenName;
        }
        this.pushCache(cacheItem);
        cache.push(cacheItem);
      });

      return cache;
    },

    generateVcardList: function(cacheList) {
      function getCardLine(handle, name) {
        return '<card handle = "' + handle + '" name = "' + name + '"/>';
      }

      const XML_HEADER = '<?xml version="1.0"?>\n' +
        '<!DOCTYPE vcard-listing SYSTEM "vcard-listing.dtd">\n' +
        '<vCard-listing version="1.0">\n';
      const XML_FOOTER = '</vCard-listing>\n';

      var lines = [];
      var count = 0;

      cacheList.forEach((item) => {
        var handle = count++ + '.vcf';
        lines.push(getCardLine(handle, item.name));
      });

      var content = XML_HEADER + lines.join('\n') + '\n' + XML_FOOTER;

      return {
        xml: content,
        size: cacheList.length
      };
    },

    getEntry: function (id) {
      switch (this._path) {
        case 'sim':
          return this._simCache[id];
        case 'ic':
          return this._phoneicCache[id];
        case 'mc':
          return this._phonemcCache[id];
        case 'oc':
          return this._phoneocCache[id];
        case 'cc':
          return this._phoneccCache[id];
      }
    },

    cleanCache: function () {
      switch (this._path) {
        case 'sim':
          this._simCache = [];
          break;
        case 'pb':
          this._phonepbCache = [];
          break;
        case 'ic':
          this._phoneicCache = [];
          break;
        case 'mc':
          this._phonemcCache = [];
          break;
        case 'oc':
          this._phoneocCache = [];
          break;
        case 'cc':
          this._phoneccCache = [];
          break;
      }
    },

    pushCache: function (item) {
      switch (this._path) {
        case 'sim':
          this._simCache.push(item);
          break;
        case 'pb':
          this._phonepbCache.push(item);
          break;
        case 'ic':
          this._phoneicCache.push(item);
          break;
        case 'mc':
          this._phonemcCache.push(item);
          break;
        case 'oc':
          this._phoneocCache.push(item);
          break;
        case 'cc':
          this._phoneccCache.push(item);
          break;
      }
    },

    getPhoneContacts: function() {
      let self = this;
      return new Promise((resolve) => {
        var contacts = [];
        var request = this._ctsMan.getAll();
        request.onsuccess = function() {
          if (this.result) {
            contacts.push(this.result);
            if (self._maxListCount === 0     ||
                self._maxListCount === 65535 ||
                self._maxListCount > contacts.length ) {
              this.continue();
            } else {
              resolve(contacts);
            }
          } else {
            resolve(contacts);
          }
        };
        request.onerror = function(error) {
          resolve(contacts);
        };
      });
    },

    getPhoneCallLog: function() {
      var self = this;
      return new Promise((resolve) => {
        var callLog = [];
        CallLogDBManager.getGroupList(function (cursor) {
          if (!cursor.value) {
            resolve(callLog);
            return;
          }
          Date.prototype.YYYYMMDD = function() {
            var YYYY = this.getFullYear().toString();
            var MM = (this.getMonth() + 1).toString();
            var DD  = this.getDate().toString();
            return YYYY + (MM[1]? MM : '0' + MM[0]) +
              (DD[1]? DD : '0' + DD[0]);
          };
          Date.prototype.hhmmss = function() {
            var hh = this.getHours().toString();
            var mm = this.getMinutes().toString();
            var ss = this.getSeconds().toString();
            return (hh[1]? hh : '0' + hh[0]) + (mm[1]? mm : '0' + mm[0]) +
              (ss[1]? ss : '0' + ss[0]);
          };

          for (var i = 0; i < cursor.value.retryCount; i++) {
            var currentCallLog = cursor.value;
            var type;
            var name;
            var tel = currentCallLog.number;

            if (currentCallLog.type === 'incoming' &&
              currentCallLog.status === 'connected') {
              type = 'ic';
            } else if (currentCallLog.type === 'incoming' &&
             currentCallLog.status === undefined) {
              type = 'mc';
            } else {
              type = 'oc';
            }
            if (type !== self._path && self._path !== 'cc') {
              break;
            }
            var currentDate = new Date(currentCallLog.calls[i].date);
            var time = currentDate.YYYYMMDD() + 'T' + currentDate.hhmmss();

            if (currentCallLog.contact) {
              name = currentCallLog.contact.primaryInfo;
            } else {
              name = 'UNKNOW NAME';
            }
            var callLogObj = {
              time: time,
              name: name,
              fn: name,
              tel: tel,
              type: type
            };
            callLog.push(callLogObj);
            if (self._maxListCount !== 0 && self._maxListCount !== 65535) {
              if (self._maxListCount <= callLog.length) {
                resolve(callLog);
                return;
              }
            }
          }
          cursor.continue();
        }, 'lastEntryDate', true, true);
      });
    },

    updatePath: function (path) {
      var type;
      if (path.indexOf('SIM1') !== -1) {
        type = 'sim';
      } else {
        if (path.indexOf('pb') !== -1) {
          type = 'pb';
        } else if (path.indexOf('ic') !== -1) {
          type = 'ic';
        } else if (path.indexOf('oc') !== -1) {
          type = 'oc';
        } else if (path.indexOf('mc') !== -1) {
          type = 'mc';
        } else if (path.indexOf('cc') !== -1) {
          type = 'cc';
        }
      }
      this._path = type;
    },

    updateMaxCount: function (maxCount) {
      if (maxCount < 0 || maxCount > 65535) {
        this._maxListCount = 65535;
      } else  {
        this._maxListCount = maxCount;
      }
    },

    getSimContacts: function() {
      return new Promise((resolve) => {
        var iccManager = navigator.mozIccManager;
        var iccid = iccManager.iccIds[0];
        var icc = iccManager.getIccById(iccid);
        var requestAdn;
        var contacts = [];

        if (icc && icc.readContacts) {
          requestAdn = icc.readContacts('adn');
        } else {
          resolve(contacts);
        }

        requestAdn.onsuccess = function() {
          contacts = requestAdn.result || [];
          resolve(contacts);
        };
        requestAdn.onerror = function() {
          resolve(contacts);
        };
      }).then(contacts =>{
        let max = 0;
        if (this._maxListCount === 0     ||
            this._maxListCount === 65535 ||
            this._maxListCount > contacts.length) {
          max = contacts.length;
        } else {
          max = this._maxListCount;
        }
        for (let i = 0; i < max; i++) {
          contacts[i].familyName = contacts[i].name;
          contacts[i].givenName = [''];
        }
        return contacts;
      });
    },

    getContactsFromPath: function() {
      switch (this._path) {
        case 'pb':
          return this.getPhoneContacts();
        case 'sim':
          return this.getSimContacts();
        case 'ic':
        case 'oc':
        case 'mc':
        case 'cc':
          return this.getPhoneCallLog().then((callLogs) => {
            return window.genCallLogObj(callLogs, this._version);
          });
        case 'unknow':
          return Promise.resolve([]);
      }
    }
  };

  exports.PbapPhonebook = PbapPhonebook;
}(window));
