/**
 * Created by tclxa on 8/15/14.
 */
'use strict';
$('menuItem-uasettings').addEventListener('click', function() {
  if(false == runnings)
    UASettings.init();
});
var runnings = false;
var UADevName = "4043S";//go flip
var path = '/system/b2g/defaults/pref/user.js';
var defvalue = {
  "uasettings":[
    {"name":"Mobile OneTouch" + UADevName,
     "comm":'pref("general.useragent.override", ' +
            '"Mozilla\/5.0 (Mobile; OneTouch' + UADevName + '; rv:28.0) ' +
            'Gecko\/28.0 Firefox\/28.0");'
     },
    {"name":"Android Mobile",
     "comm":'pref("general.useragent.override", ' +
            '"Mozilla\/5.0 (Android; Mobile; rv:28.0) ' +
            'Gecko\/28.0 Firefox\/28.0");'
    },
    {"name":"Mobile ALCATEL" + UADevName,
     "comm":'pref("general.useragent.override", ' +
            '"Mozilla\/5.0 (Mobile; ALCATEL' + UADevName + '; rv:28.0) ' +
            'Gecko\/28.0 Firefox\/28.0");'
    },
    {"name":"Mobile ALCATELOneTouch",
     "comm":'pref("general.useragent.override", ' +
            '"Mozilla\/5.0 (Mobile; ALCATELOneTouch; rv:28.0) ' +
            'Gecko\/28.0");'
    }
  ]
};

var UASettings = {
  init: function init(){
    runnings = true;
    var engmodeEx = navigator.engmodeExtension;
    function newItems(defvalue){
      for(var i = 0; i < defvalue.length; i++){
        var a = document.createElement('a');
        a.id = 'a1' + i;
        a.innerHTML = defvalue[i].name;
        a.title = defvalue[i].comm;
        var li = document.createElement('li');
        li.appendChild(a);
        $('uasettingslist').appendChild(li);
        $(a.id).tabIndex = 1;
        $(a.id).jrdFocus = true;
        $(a.id).addEventListener('click', function(e) {
          if(engmodeEx){
            var initRequest = engmodeEx.fileWriteLE(e.target.title, path, 'a');
            initRequest.onsuccess = function(){
              $('inputcomm').innerHTML = '<span>' + e.target.title +
                '<br>' + '</span>';
              restart();
            }
          }
        });
        $('uscustom-write').addEventListener('click', function(e) {
          var inputValue = $('uscustom-vaule-input').value;
          var customcomm = 'pref("general.useragent.override", "' +
            inputValue + '");';
          if(engmodeEx){
            var initRequest = engmodeEx.fileWriteLE(customcomm, path, 'a');
            initRequest.onsuccess = function(){
              $('uacustominputcomm').innerHTML =
                '<span>' + customcomm + '<br>' + '</span>';
              restart();
            }
          }

        });
      }
    }
    newItems(defvalue.uasettings);
    function restart(){
      alert('This Phone will be Restart!');
      if(engmodeEx){
        window.setTimeout(function(){
          var parmArray = new Array();
          parmArray.push('engmodereboot');
          var initRequest = engmodeEx.execCmdLE(parmArray, 1);
        },3000);
      }
    }
  }
}
