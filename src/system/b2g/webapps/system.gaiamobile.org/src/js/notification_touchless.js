'use strict';
var NotificationTouchless = {
  EVENT_PREFIX: 'NotificationTouchless',
  name: 'NotificationTouchless',
  screenEnabled:true,
  keypanel: undefined,
  screen_width: 0,
  screen_height: 0,
  active: false,
  shown: false,
  musicStaus:'',
  activOption: undefined,

  screen:  document.getElementById('screen'),
  nt_overlay: document.getElementById('touchless-notification-overlay'),
  container: document.getElementById('touchless-desktop-notifications-container'),
  noneTip:   document.getElementById('touchless-none-tip'),

  notifications:     document.getElementById('touchless-notifications'),
  notificationTitle: document.getElementById('touchless-notification-some'),

  handleEvent: function _handleEvent(evt) {
    switch (evt.type) {
      case 'attentionopened':
        var manifest = evt.detail.manifestURL;
        if (manifest == "app://network-alerts.gaiamobile.org/manifest.webapp"
          || manifest == "app://callscreen.gaiamobile.org/manifest.webapp"
          || manifest == "app://clock.gaiamobile.org/manifest.webapp") {
          if (this.shown) {
            this.hide();
          }
          document.querySelector('[mozapp="' + manifest + '"]:not(.hidden)').focus();
        }
      break;
      case 'imemenushow':
        this.hide();
        break;
      case 'cardviewbeforeshow':
        this.hide(true);
        break;

      case 'screenchange':
        if (this.shown && !evt.detail.screenEnabled)
        {
          this.hide(true);
        }
        this.screenEnabled = evt.detail.screenEnabled;
        break;

      case 'transitionend':
        if (!this.shown)
        {
          this.screen.classList.remove('utility-tray');
        }
        break;

      case 'appopening':
      case 'simpinshow':
      case 'emergencyalert':
        if (this.shown) {
          this.hide();
        }
        break;

      case 'mozChromeEvent':
        if (evt.detail.type !== 'accessibility-control')
        {
          break;
        }
        break;
    }
  },

  publish: function(evtName) {
    window.dispatchEvent(new CustomEvent(this.EVENT_PREFIX + evtName,
    {
      detail: this
    }));
  },

  respondToHierarchyEvent: function (evt) {
    return this['_handle_' + evt.type] ? this['_handle_' + evt.type](evt) : true;
  },

  getActiveWindow: function() {
    return this.isActive()? this:null;
  },
  getTopMostWindow: function() {
    return this.isActive()? this:null;
  },
  init: function _init() {
    var nt_container = document.getElementById('notifications-container');
    var self = this;
    var observer = new MutationObserver(function (mutations) {
      if (NotificationTouchless.isActive())
      {
        var number = mutations.length;
        for (var i = 0; i < number; ++i) {
          if (mutations[i].type == "childList") {
            if (mutations[i].removedNodes.length !== 0 || mutations[i].addedNodes.length !== 0) {
              var detail = { type: null, node: null };
              detail.type = (mutations[i].removedNodes.length !== 0) ? 'deleted' : 'added';
              detail.node = (detail.type === 'deleted') ? mutations[i].removedNodes[0] : mutations[i].addedNodes[0];
              if (detail.node.classList && detail.node.classList.contains("notification")) {
                self.updateNotificationCount(detail);
              }
            }
          } else if (mutations[i].type === 'attributes') {
            if (mutations[i].target.classList.contains("media-notification")) {
              if ((mutations[i].target.style.getPropertyValue('display') !== 'none') && mutations[i].target.classList.contains('focus')) {
                  self.updateSoftKey(mutations[i].target);
              }
              self.updateNotificationCount();
            } else if (mutations[i].target.classList.contains("fake-notification")) {
              if (mutations[i].attributeName === 'class') {
                if (mutations[i].target.classList.contains('displayed')) {
                  if(mutations[i].target.classList.contains('focus')){
                    self.updateSoftKey(mutations[i].target);
                  }
                  self.updateNotificationCount();
                } else if(mutations[i].oldValue.contains('displayed')){
                  self.updateNotificationCount();
                }
              }
            } else if (mutations[i].target.classList.contains("notification")) {
              if (mutations[i].attributeName === 'class' && mutations[i].target.classList.contains('focus')) {
                self.updateSoftKey(mutations[i].target);
              }
            }
          }
        }
      }
    });

    observer.observe(nt_container, { subtree: true, attributes: true, childList: true, attributeOldValue:true});

    window.addEventListener('cardviewbeforeshow', this);
    window.addEventListener('screenchange', this);
    window.addEventListener('emergencyalert', this);
    window.addEventListener('attentionopened', this);
    window.addEventListener('launchapp', this);
    window.addEventListener('appopening', this);


    // Listen for screen reader edge gestures
    window.addEventListener('mozChromeEvent', this);

    // Firing when the keyboard and the IME switcher shows/hides.
    window.addEventListener('keyboardimeswitchershow', this);
    window.addEventListener('keyboardimeswitcherhide', this);
    window.addEventListener('imemenushow', this);
    window.addEventListener('iac-mediacomms', this.handleMessage.bind(this));
    window.addEventListener('iac-notice', this.show.bind(this));
    this.nt_overlay.addEventListener('transitionend', this);



    window.addEventListener("keydown", this.handKeyDown.bind(this));

    Service.request('registerHierarchy', this);

    Service.register('show', this);

    this.initSoftKey(this.attentionOption);
  },


  handleMessage: function _handleMessage(event) {
    var message = event.detail;
    switch (message.type) {
      case 'status':
        this.musicStaus = message.data.playStatus;
        break;
    }
  },

  handKeyDown: function _handleKeyDown(evt) {
    switch (evt.key) {
      case 'Notification': // GO flip
      case 'F3': // Pixi
      case 'Alt': // Emulator
        break;

      case 'BrowserBack':
      case 'MozHomeScreen':
      case 'Home':
        if (this.shown) {
          this.hide();
          evt.preventDefault();
        }
      break;
      case 'Clear':
      case 'Backspace':
        if (this.shown) {
          if (NotificationTouchless.findFocusedNotification() && this.activOption !== this.attentionOption)
          {
            NotificationTouchless.showToaster('tcl-ntf-toaster-clear');
            NotificationScreen.closeFocusedNotification();
          }
        }
        break;
      default:
        break;
    }
  },

  initSoftKey: function (Option) {
    if (!this.keypanel){
      this.keypanel = new SoftkeyPanel(Option);
    }
  },

  updateOption: function(Option){
    this.activOption = Option;
    this.keypanel.initSoftKeyPanel(Option);
  },

  hideSoftkeyPanel: function(){
    if(this.keypanel)
    {
      this.keypanel.hide();
    }
  },

  showSoftkeyPanel: function(){
    if(this.keypanel)
    {
      this.keypanel.show();
    }
  },

  updateSoftKey: function(notificationNode) {
    if (!notificationNode) {
      this.updateOption(this.noneOption);
      return;
    }

    var notificationClassList = notificationNode.classList;

    if (notificationClassList.contains('attention-notification') || notificationClassList.contains('fake-notification'))
    {
      this.updateOption(this.attentionOption);
    }
    else if (notificationClassList.contains('media-notification')) {
      if (this.musicStaus === 'PLAYING')
        this.updateOption(this.musicPlayOption);
      else if (this.musicStaus === 'PAUSED')
        this.updateOption(this.musicPauseOption);
    }
    else {
      this.updateOption(this.normalOption);
    }
  },



  validateCachedSizes: function(refresh) {
    var screenRect;
    if (refresh || !this.screen_height || !this.screen_width) {
      screenRect = this.nt_overlay.getBoundingClientRect();
    }

    if (refresh || !this.screen_width) {
      this.screen_width = screenRect.width || 0;
    }

    if (refresh || !this.screen_height) {
      this.screen_height = screenRect.height || 0;
    }

  },

  hide: function _hide(instant) {
    if (!this.active) {
      window.dispatchEvent(new CustomEvent('utilitytraywillhide'));
    }

    this.validateCachedSizes();
    var alreadyHidden = !this.shown;
    var style = this.nt_overlay.style;

    this.screen.classList.remove('utility-tray');

    this.shown = false;
    window.dispatchEvent(new CustomEvent(this.EVENT_PREFIX + '-deactivating'));

    if (!alreadyHidden) {
      var evt = document.createEvent('CustomEvent');
      evt.initCustomEvent('utilitytrayhide', true, true, null);
      window.dispatchEvent(evt);
    }

    // hide softkey panel
    this.hideSoftkeyPanel();
    this.sendPanelUnfocusedEvent();
  },

  show: function _show(instant) {

    this.validateCachedSizes();
    var alreadyShown = this.shown;
    if (alreadyShown){
      this.hide();
      return;
    }

    this.updateNotificationCount();
    this.showSoftkeyPanel();
    this.sendPanelFocusedEvent();

    var style = this.nt_overlay.style;
    style.MozTransition = instant ? '' : '-moz-transform 0.2s linear';
    style.MozTransform = 'translateY(100%)';
    this.notifications.style.transition =
      instant ? '' : 'transform 0.2s linear';
    this.notifications.style.transform = '';

    this.shown = true;
    this.screen.classList.add('utility-tray');
    window.dispatchEvent(new CustomEvent(this.EVENT_PREFIX + '-activating'));
    var focusedNotificationNode = this.findFocusedNotification();
    this.updateSoftKey(focusedNotificationNode);

    if (!alreadyShown) {
      var evt = document.createEvent('CustomEvent');
      evt.initCustomEvent('utilitytrayshow', true, true, null);
      window.dispatchEvent(evt);
      Service.currentApp.blur();
    }
  },

  updateNotificationCount: function _updateNotificationCount(detail) {

    var count = NT_DOM_HELPER.getNotificationCount();

    navigator.mozL10n.setAttributes(this.notificationTitle,'tcl-ntf-title', {n: count});

    if(count > 0){
      var focusedNotificationNode = this.findFocusedNotification();
      this.noneTip.classList.add("hide");
      this.updateSoftKey(focusedNotificationNode);
    } else {
      this.updateSoftKey();
      this.noneTip.classList.remove("hide");
    }

    this.sendPanelUpdateEvent(detail);
  },

  sendPanelUpdateEvent: function (detail) {
    var event = new CustomEvent('nt_notification_panel_update', { detail: detail });
    window.dispatchEvent(event);
  },

  sendPanelUnfocusedEvent:function(){
    var event = new CustomEvent('nt_notification_panel_unfocused');
    window.dispatchEvent(event);
  },

  sendPanelFocusedEvent : function(){
    var event = new CustomEvent('nt_notification_panel_focused');
    window.dispatchEvent(event);
  },

  findFocusedNotification: function _findFocusedNotification() {
    return  NT_DOM_HELPER.getFocusedNotification();
  },

  showToaster: function (l10msg) {
    var toast = {
      messageL10nId: l10msg,
      latency: 2000,
      useTransition: true
    };

    var event = new CustomEvent('systoastershow', { detail: toast });
    window.dispatchEvent(event);
  },

  _pdIMESwitcherShow: function _pdIMESwitcherShow(evt) {
    if ('rocketbar-input' !== evt.target.id) {
      evt.preventDefault();
    }
  },

  isActive: function () {
    return this.shown;
  },

  noneOption:{
    menuClassName: 'menu-button',
    header: { l10nId: 'message' },
    items: [
      {
        name: '',
        priority: 2,
        method:function(){}
      },
    ]
  },

  normalOption: {
    menuClassName: 'menu-button',
    header: { l10nId: 'message' },
    items: [
      {
        // Dismiss
        name: 'Dismiss',
        l10nId: 'tcl-ntf-dismiss',
        priority: 1,
        method: function () {
          if (NotificationTouchless.isActive())
          {
            NotificationTouchless.showToaster('tcl-ntf-toaster-clear');
            NotificationScreen.closeFocusedNotification();
          }
        }
      },
      {
        name: '',
        priority: 2,
        method: function () {
          if (NotificationTouchless.isActive())
            NotificationScreen.tap(NotificationTouchless.findFocusedNotification());
        }
      },
      {
        // Dismiss All
        name: 'Dismiss All',
        l10nId: 'tcl-ntf-dismiss-all',
        priority: 3,
        method: function () {
          if (NotificationTouchless.isActive())
          {
            NotificationTouchless.showToaster('tcl-ntf-toaster-clear-all');
            NotificationScreen.clearAll();
          }
        }
      }
    ]
  },

  musicPlayOption: {
    menuClassName: 'menu-button',
    header: { l10nId: 'message' },
    items: [
      {
        // Open
        name: 'Open',
        l10nId: 'tcl-ntf-open',
        priority: 2,
        method: function () {
          if (NotificationTouchless.isActive())
            NotificationScreen.tap(NotificationTouchless.findFocusedNotification());
        }
      },
      {
        // play
        name: 'Pause',
        l10nId: 'tcl-ntf-pause',
        priority: 3,
        method: function () {
          if (NotificationTouchless.isActive())
            NotificationScreen.sendMediaCommand("pause");
        }
      },
    ]
  },

  musicPauseOption: {
    menuClassName: 'menu-button',
    header: { l10nId: 'message' },
    items: [
      {
        // Open
        name: 'Open',
        l10nId: 'tcl-ntf-open',
        priority: 2,
        method: function () {
          if (NotificationTouchless.isActive())
            NotificationScreen.tap(NotificationTouchless.findFocusedNotification());
        }
      },

      {
        // play
        name: 'Play',
        l10nId: 'tcl-ntf_play',
        priority: 3,
        method: function () {
          if (NotificationTouchless.isActive())
            NotificationScreen.sendMediaCommand("play");
        }
      },
    ]
  },

  attentionOption: {
    menuClassName: 'menu-button',
    header: { l10nId: 'message' },
    items: [
      {
        name:   "Open",
        l10nId: 'tcl-ntf-open',
        priority:2,
        method: function () {
          if (NotificationTouchless.isActive())
            NotificationScreen.tap(NotificationTouchless.findFocusedNotification());
        }
      }
    ]
  },
};
