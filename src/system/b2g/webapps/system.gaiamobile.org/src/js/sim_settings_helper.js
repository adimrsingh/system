/* exported SimSettingsHelper */
/* global SIMSlotManager, SettingsListener */
'use strict';

(function (exports) {
  // we have to make sure we are in DSDS
  if (!SIMSlotManager.isMultiSIM()) {
    return;
  }

  var SimSettingsHelper = {
    ALWAYS_ASK_OPTION_VALUE: -1,
    initFromDB: false,
    iccIds: [null, null],
    notification: null,
    start: function ssh_init() {
      if (SIMSlotManager.ready) {
        this.initSimSettings();
      } else {
        window.addEventListener('simslotready', this.initSimSettings.bind(this));
      }
      this.observeUserSimSettings();
    },

    initSimSettings: function ssh_recordCurrentIccIds(evt) {
      var conns = window.navigator.mozMobileConnections;
      for (var i = 0; i < conns.length; i++) {
        this.iccIds[i] = conns[i].iccId;
      };
      if (this.initFromDB) {
        this.simslotUpdatedHandler();
      }
    },

    overrideUserSimSettings: function () {
      this.setServiceOnCard('outgoingCall');
      this.setServiceOnCard('outgoingMessages');
      this.setServiceOnCard('outgoingData');
    },

    simslotUpdatedHandler: function () {
      this.overrideUserSimSettings();
      if (!this['ril.notFirst.sim.settings']) {
        SettingsListener.getSettingsLock().set({
          'ril.notFirst.sim.settings': true
        });
      }
    },

    //bug2774-add by deming.wu-start
    onCustomizationVolte: function ssh_onCustomizationVolte() {
      dump('onCustomizationVolte : sim card has changed.');
      var simmcc, simmnc;
      simmcc = IccHelper.iccInfo.mcc;
      simmnc = IccHelper.iccInfo.mnc;
      dump('onCustomizationVolte : cureent simmcc = ' + simmcc + ' ; simmnc = ' + simmnc);
      // 0(default): menu hidden, toggle off
      // 1: menu display, toggle on
      // 2: menu hidden, toggle on
      var volteMccmncValue = 0;
      if (simmcc === '204') {
        if (simmnc === '02') {
          volteMccmncValue = 0;
        } else if (simmnc === '04') {
          volteMccmncValue = 2;
        } else if (simmnc === '16' || simmnc === '20') {
          volteMccmncValue = 1;
        }
      } else if (simmcc === '202' && simmnc === '01') {
        volteMccmncValue = 1;
      } else if (simmcc === '208') {
        if (simmnc === '01' || simmnc === '20') {
          volteMccmncValue = 0;
        } else {
          volteMccmncValue = 1;
        }
      } else if (simmcc === '214' && simmnc === '01') {
        volteMccmncValue = 2;
      } else if (simmcc === '216' && simmnc === '30') {
        volteMccmncValue = 1;
      } else if (simmcc === '219' && simmnc === '01') {
        volteMccmncValue = 1;
      } else if (simmcc === '222' && (simmnc === '01' || simmnc === '10')) {
        volteMccmncValue = 2;
      } else if (simmcc === '226' && (simmnc === '03'|| simmnc === '06')) {
        volteMccmncValue = 0;
      } else if (simmcc === '228' && simmnc === '01') {
        volteMccmncValue = 1;
      } else if (simmcc === '230') {
        if (simmnc === '01') {
          volteMccmncValue = 1;
        } else if (simmnc === '03') {
          volteMccmncValue = 2;
        }
      } else if (simmcc === '231' && simmnc === '02') {
        volteMccmncValue = 1;
      } else if (simmcc === '232' && (simmnc === '03'  || simmnc === '01')) {
        volteMccmncValue = 1;
      } else if (simmcc === '234') {
        if (simmnc === '30' || simmnc === '33') {
          volteMccmncValue = 2;
        }
      } else if (simmcc === '260' && simmnc === '02') {
        volteMccmncValue = 1;
      } else if (simmcc === '262') {
        if (simmnc === '01') {
          volteMccmncValue = 1;
        } else if (simmnc === '02') {
          volteMccmncValue = 2;
        }
      } else if (simmcc === '268' && simmnc === '01') {
        volteMccmncValue = 2;
      } else if (simmcc === '286') {
        volteMccmncValue = 1;
    } else if (simmcc === '454' || simmcc === '450') {
        volteMccmncValue = 1;
      } else if (simmcc === '460') {
        volteMccmncValue = 1;
      } else if (simmcc === '466') {
      if (simmnc === '01' || simmnc === '05' || simmnc === '89') {
        volteMccmncValue = 1;
      } else if (simmnc === '92') {
        volteMccmncValue = 3;
      } else {
        volteMccmncValue = 0;
        }
      } else if (simmcc === '502') {
        volteMccmncValue = 1;
      } else if (simmcc === '520') {
        volteMccmncValue = 1;
      } else if (simmcc === '525') {
        //bug5040-add by deming.wu-start
        if (simmnc === '01' || simmnc === '05') {
          volteMccmncValue = 0;
        } else {
          //bug5040-add by deming.wu-end
          volteMccmncValue = 1;
        }
      } else if (simmcc === '602' && simmnc === '03') {
	  volteMccmncValue = 1;
      } else if (simmcc === '655') {
        volteMccmncValue = 3;
      } else if (simmcc === '424') {
        volteMccmncValue = 1;
      }

      navigator.mozMobileConnections[0].imsHandler.setEnabled(!!volteMccmncValue);
      if (volteMccmncValue === 1) {
        dump('onCustomizationVolte : volte menu show & toggle on');
        SettingsListener.getSettingsLock().set({
          'volte_vowifi_settings.show': true
        });
        SettingsListener.getSettingsLock().set({
          'ril.ims.enabled': true
        });
        SettingsListener.getSettingsLock().set({
          'ril.ims.preferredProfile': 'cellular-only'
        });
      } else if (volteMccmncValue === 2){
        dump('onCustomizationVolte : volte menu hidden & toggle on');
        SettingsListener.getSettingsLock().set({
          'volte_vowifi_settings.show': false
        });
        SettingsListener.getSettingsLock().set({
          'ril.ims.enabled': true
        });
        SettingsListener.getSettingsLock().set({
          'ril.ims.preferredProfile': 'cellular-only'
        });
      } else if (volteMccmncValue === 3){
        dump('onCustomizationVolte : volte menu show & toggle OFF');
        SettingsListener.getSettingsLock().set({
          'volte_vowifi_settings.show': true
        });
        SettingsListener.getSettingsLock().set({
          'ril.ims.enabled': false
        });
        SettingsListener.getSettingsLock().set({
          'ril.ims.preferredProfile': 'cellular-only'
        });
      } else {
        dump('onCustomizationVolte : volte menu hidden & toggle off');
        SettingsListener.getSettingsLock().set({
          'volte_vowifi_settings.show': false
        });
        SettingsListener.getSettingsLock().set({
          'ril.ims.enabled': false
        });
      }
    },

    //bug2774-add by deming.wu-end
    showSimCardConfirmation: function (cardIndex) {
      if (this.notification) {
        this.notification.close();
        this.notification = null;
      }
      if (SIMSlotManager.noSIMCardOnDevice() ||
        !this['ril.notFirst.sim.settings']) {
        return null;
      }
      this.onCustomizationVolte();//bug2774-add by deming.wu
      var _ = navigator.mozL10n.get;
      var title = _('sim-confirmation-title') || '';
      var body = _('sim-confirmation-notice') || '';

      var notification = new window.Notification(title, {
        body: body,
        tag: 'simCard',
        data: {
          icon: 'sim-card'
        },
        mozbehavior: {
          showOnlyOnce: true
        }
      });

      notification.onclick = function (cardIndex) {
        var _ = navigator.mozL10n.get;
        var header = _('sim-confirmation-title');
        var content = _('sim-confirmation-content', {
          'n': cardIndex + 1
        });
        if (this.notification) {
          this.notification.close();
          this.notification = null;
        }

        Service.request('DialogService:show', {
          header: header,
          content: content,
          translated: true,
          type: 'alert',
          onOk: () => { }
        });
      }.bind(this, cardIndex);
      return notification;
    },

    observeUserSimSettings: function ssh_observeUserSimSettings() {
      var mozKeys = [];
      var Settings = navigator.mozSettings;
      var servicePromises = [];
      mozKeys.push('ril.telephony.defaultServiceId');
      mozKeys.push('ril.voicemail.defaultServiceId');
      mozKeys.push('ril.telephony.defaultServiceId.iccId');
      mozKeys.push('ril.voicemail.defaultServiceId.iccId');

      mozKeys.push('ril.sms.defaultServiceId');
      mozKeys.push('ril.sms.defaultServiceId.iccId');

      mozKeys.push('ril.mms.defaultServiceId');
      mozKeys.push('ril.data.defaultServiceId');
      mozKeys.push('ril.mms.defaultServiceId.iccId');
      mozKeys.push('ril.data.defaultServiceId.iccId');
      mozKeys.push('ril.notFirst.sim.settings');
      mozKeys.push('ril.sim.iccIds');

      mozKeys.forEach((eachKey) => {
        var promise = new Promise((resolve) => {
          var lock = navigator.mozSettings.createLock();
          var request = lock.get(eachKey);
          request.onsuccess = () => {
            this[eachKey] = request.result[eachKey];
            resolve();
          };
          request.onerror = () => {
            resolve();
          }
          Settings.addObserver(eachKey, function onChange(event) {
            var value = event.settingValue;
            // 1. 'newPrefCard' --> set data to other card
            // 2. 'oldPrefCard' --> reboot device and card not change
            // 3. 'recordPrefCard' -->  insert new card and the card is the last
            //                          card that user set mobile data
            // 4. 'noSimCard' --> no sim card in device
            // 5. 'noMobileData' --> the card not set mobile data.
            if (eachKey === 'ril.data.defaultServiceId' &&
              Service.query('supportSwitchPrimarysim')) {
              var cardsState = [{
                state: 'newPrefCard'
              }, {
                state: 'newPrefCard'
              }];
              cardsState[value ^ 1].state = 'noMobileData';
              // No simCard in device
              if (!this.iccIds[value]) {
                cardsState[value].state = 'noSimCard';
              } else if (this.iccIds[value] ===
                this['ril.data.defaultServiceId.iccId']) {
                cardsState[value].state = 'recordPrefCard';
              } else if (this['ril.sim.iccIds'][value] === this.iccIds[value] &&
                this[eachKey] === value) {
                cardsState[value].state = 'oldPrefCard';
              }
              Service.request('setPreferredNetworkType', cardsState);
            }

            this[eachKey] = value;
            // For support SIM1* SIM2* mode, record
            // ril.data.defaultServiceId.iccID in settings app
            if (eachKey === 'ril.data.defaultServiceId.iccId') {
              if (this.notification) {
                this.notification.close();
                this.notification = null;
              }
            } else if (eachKey.indexOf('iccId') < 0 && value >= 0 &&
              (eachKey !== 'ril.data.defaultServiceId' ||
                !this['ril.notFirst.sim.settings'])) {
              var setObj = {};
              setObj[eachKey + '.iccId'] = this.iccIds[value];
              SettingsListener.getSettingsLock().set(setObj);
            }
          }.bind(this));
        });
        servicePromises.push(promise);
      });
      Promise.all(servicePromises).then(() => {
        this.initFromDB = true;
        if (SIMSlotManager.ready || SIMSlotManager.noSIMCardOnDevice()) {
          this.simslotUpdatedHandler();
        }
      });
    },

    setServiceOnCard: function ssh_setServiceOnCard(serviceName) {
      var mozKeys = [];
      var notFirstSet = this['ril.notFirst.sim.settings'];
      var cardIndex = this.ALWAYS_ASK_OPTION_VALUE;
      switch (serviceName) {
        case 'outgoingCall':
          mozKeys.push('ril.telephony.defaultServiceId');
          mozKeys.push('ril.voicemail.defaultServiceId');
          break;

        case 'outgoingMessages':
          mozKeys.push('ril.sms.defaultServiceId');
          mozKeys.push('ril.mms.defaultServiceId');
          break;

        case 'outgoingData':
          mozKeys.push('ril.data.defaultServiceId');
          break;
      }
      // Delete for bug4376 lina.zhang@t2mobile.com 20180420 -begin
      // First time run and first set.
      // if (!notFirstSet) {
      //   if (SIMSlotManager.noSIMCardOnDevice()) {
      //     cardIndex = this.ALWAYS_ASK_OPTION_VALUE;
      //   } else {
      //     cardIndex = SIMSlotManager.isSIMCardAbsent(0) ? 1 : 0;
      //   }
      // }
      // Delete for bug4376 lina.zhang@t2mobile.com 20180420 -end

      mozKeys.forEach((eachKey) => {
        if (cardIndex === this.ALWAYS_ASK_OPTION_VALUE && notFirstSet) {
          // Not first set, need compare remembered iccid
          // if not matched, then set to -1. (always ask)
          var iccId = this[eachKey + '.iccId'];
          if (iccId) {
            if (iccId === this.iccIds[0]) {
              cardIndex = 0;
            } else if (iccId === this.iccIds[1]) {
              cardIndex = 1;
            }
          }
        }
        if (eachKey === 'ril.data.defaultServiceId') {
          if (cardIndex === this.ALWAYS_ASK_OPTION_VALUE) {
            // 1. No simcard
            // 2. slot1 empty & slot2 new simcard
            // 3. slot1 new simcard & slot2 empty
            // 4. slot1 & slot2 both new simcards
            // 1, 3, 4 cardIndex set to 0, else set to 1
            var slots = SIMSlotManager.getSlots();
            if (slots[0].isAbsent() && !slots[1].isAbsent()) {
              cardIndex = 1;
            } else {
              cardIndex = 0;
            }
          }
          if (!this['ril.sim.iccIds'] ||
            this['ril.sim.iccIds'][cardIndex] !== this.iccIds[cardIndex] ||
            this['ril.data.defaultServiceId'] !== cardIndex) {
            this.notification = this.showSimCardConfirmation(cardIndex);
          }
          var iccIdsSetObj = {};
          iccIdsSetObj['ril.sim.iccIds'] = this.iccIds;
          SettingsListener.getSettingsLock().set(iccIdsSetObj);
        }
        var setObj = {};
        setObj[eachKey] = cardIndex;
        SettingsListener.getSettingsLock().set(setObj);
      });
    }
  };
  exports.simSettingsHelper = SimSettingsHelper;
})(window);
