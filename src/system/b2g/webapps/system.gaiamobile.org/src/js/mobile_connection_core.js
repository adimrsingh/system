/* global BaseModule, SimSettingsHelper, SIMSlotManager */
'use strict';

(function() {
  // Responsible to load and init the sub system for mobile connections.
  var MobileConnectionCore = function(mobileConnections, core) {
    this.core = core;
    this.mobileConnections = mobileConnections;
  };
  //MobileConnectionCore.IMPORTS = [
  //  'shared/js/simslot.js',
  //  'shared/js/simslot_manager.js'
  //];
  MobileConnectionCore.SUB_MODULES = [
    'Radio',
    'CallForwarding',
    'EmergencyCallbackManager',
    'EuRoamingManager',
    'SimLockManager',
    'TelephonySettings',
    'OperatorVariantManager',
    'InternetSharing',
    'CellBroadcastSystem'
  ];

  MobileConnectionCore.SERVICES = [
    'showWifiCallNotification',
    'clearWifiCallNotification'
  ];

  BaseModule.create(MobileConnectionCore, {
    name: 'MobileConnectionCore',

    _start: function() {
      // we have to make sure we are in DSDS
      if (SIMSlotManager.isMultiSIM()) {
        BaseModule.lazyLoad(['simSettingsHelper']).then(function() {
          this.debug('lazily load simSettingsHelper');
          this.simSettingsHelper = simSettingsHelper;
          this.simSettingsHelper.start();
        }.bind(this));
      }
      this.clearWifiCallNotification();
    },

    // XXX: move to standalone module
    showWifiCallNotification: function(index) {
      var imsRegHandler = navigator.mozMobileConnections[index].imsHandler;
      var _ = navigator.mozL10n.get;
      var icon = window.location.protocol + '//' + window.location.hostname +
        '/style/icons/captivePortal.png';
      var notification = new Notification('', {
        body: _('wifiCallErrorMsg', {error: imsRegHandler.unregisteredReason}),
        icon: icon,
        tag: 'wificall',
        mozbehavior: {
          showOnlyOnce: true
        }
      });
      notification.onclick = function() {
        var activity = new MozActivity({
          name: 'configure',
          data: {
            target: 'device',
            section: 'volte'
          }
        });
        notification.close();
      }.bind(this);
    },

    clearWifiCallNotification: function() {
      Notification.get().then(function(notifications) {
        notifications.forEach(function(notification) {
          var tag = notification && notification.tag;
          if (!tag || tag !== 'wificall') {
            return;
          }
          notification.close();
        });
      });
    }
  });
}());
