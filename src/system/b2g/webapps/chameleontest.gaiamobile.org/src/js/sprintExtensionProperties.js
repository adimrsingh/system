var sysprops = [
  {name:"SYSPROP_NETWORK_MODE", id:657},
  {name:"SYSPROP_SELLER_NAME", id:420},
  {name:"Chameleon_SYSPROP_PAYLOAD_CUSTOMID", id:620},
  {name:"Chameleon_SYSPROP_BROWSER_HOME_URL", id:8},
  {name:"Chameleon_SYSPROP_OPERATORID", id:500},
  {name:"Chameleon_SYSPROP_TETHEREDDATA_ENABLED", id:505},
  {name:"Chameleon_SYSPROP_UTUBE", id:0},
  {name:"Chameleon_SYSPROP_1STCALLINTERCEPT", id:549},
  {name:"Chameleon_SYSPROP_2NDCALLINTERCEPT", id:550},
  {name:"Chameleon_SYSPROP_3RDCALLINTERCEPT", id:551},
  {name:"Chameleon_SYSPROP_4THCALLINTERCEPT", id:552},
  {name:"Chameleon_SYSPROP_5THCALLINTERCEPT", id:553},
  {name:"Chameleon_SYSPROP_6THCALLINTERCEPT", id:554},
  {name:"Chameleon_SYSPROP_7THCALLINTERCEPT", id:555},
  {name:"Chameleon_SYSPROP_8THCALLINTERCEPT", id:556},
  {name:"Chameleon_SYSPROP_9THCALLINTERCEPT", id:557},
  {name:"Chameleon_SYSPROP_10THCALLINTERCEPT", id:558},
  {name:"Chameleon_SYSPROP_11THCALLINTERCEPT", id:559},
  {name:"Chameleon_SYSPROP_12THCALLINTERCEPT", id:560},
  {name:"Chameleon_SYSPROP_1STCNTCTINFO", id:561},
  {name:"Chameleon_SYSPROP_2NDCNTCTINFO", id:562},
  {name:"Chameleon_SYSPROP_3RDCNTCTINFO", id:563},
  {name:"Chameleon_SYSPROP_4THCNTCTINFO", id:564},
  {name:"Chameleon_SYSPROP_5THCNTCTINFO", id:565},
  {name:"Chameleon_SYSPROP_6THCNTCTINFO", id:566},
  {name:"Chameleon_SYSPROP_1STADCINFO", id:567},
  {name:"Chameleon_SYSPROP_2NDADCINFO", id:568},
  {name:"Chameleon_SYSPROP_3RDADCINFO", id:569},
  {name:"Chameleon_SYSPROP_4THADCINFO", id:570},
  {name:"Chameleon_SYSPROP_5THADCINFO", id:571},
  {name:"Chameleon_SYSPROP_6THADCINFO", id:572},
  {name:"Chameleon_SYSPROP_7THADCINFO", id:573},
  {name:"Chameleon_SYSPROP_8THADCINFO", id:574},
  {name:"Chameleon_SYSPROP_9THADCINFO", id:575},
  {name:"Chameleon_SYSPROP_10THADCINFO", id:576},
  {name:"Chameleon_SYSPROP_11THADCINFO", id:577},
  {name:"Chameleon_SYSPROP_12THADCINFO", id:578},
  {name:"Chameleon_SYSPROP_13THADCINFO", id:579},
  {name:"Chameleon_SYSPROP_14THADCINFO", id:580},
  {name:"Chameleon_SYSPROP_15THADCINFO", id:581},
  {name:"Chameleon_SYSPROP_16THADCINFO", id:582},
  {name:"Chameleon_SYSPROP_17THADCINFO", id:583},
  {name:"Chameleon_SYSPROP_18THADCINFO", id:584},
  {name:"Chameleon_SYSPROP_DIAGMSLREQ", id:587},
  {name:"Chameleon_SYSPROP_EMAIL_SIGNATURE", id:593},
  {name:"Chameleon_SYSPROP_ROAMHOMEONLY", id:586},
  {name:"Chameleon_SYSPROP_ROAMMENUDISPLAY", id:585},
  {name:"Chameleon_SYSPROP_ROAMMENU", id:639},
  {name:"Chameleon_SYSPROP_ENABLEDAPPS", id:0},
  {name:"Chameleon_SYSPROP_SPEEDDIAL_1", id:637},
  {name:"Chameleon_SYSPROP_LAUNCHWIZARD", id:0},
  {name:"Chameleon_SYSPROP_GPSONE_PDEIP", id:641},
  {name:"Chameleon_SYSPROP_GPSONE_PDEPORT", id:642},
  {name:"Chameleon_SYSPROP_MMS_AUTORETRIEVE_ENABLED", id:646},
  {name:"Chameleon_SYSPROP_MMS_HTTPHEADER", id:647},
  {name:"Chameleon_SYSPROP_OMADM_CICM", id:649},
  {name:"SPA_SYSPROP_DOMDATA_ROAMGUARD", id:72},
  {name:"SPA_SYSPROP_INTLDATA_ROAMGUARD", id:424},
  {name:"SPA_SYSPROP_DOMDATA_ROAMGUARD_FORCED", id:476},
  {name:"SPA_SYSPROP_BARDOMDATA_ROAM", id:477},
  {name:"SPA_SYSPROP_DOMDATA_ROAM_FORCED", id:478},
  {name:"SPA_SYSPROP_DOMVOICE_ROAMGUARD", id:479},
  {name:"SPA_SYSPROP_INTLDATA_ROAMGUARD_FORCED", id:484},
  {name:"SPA_SYSPROP_INTLDATA_ROAM", id:485},
  {name:"SPA_SYSPROP_INTLDATA_ROAM_FORCED", id:486},
  {name:"SPA_SYSPROP_BARINTLVOICE_ROAM_FORCED", id:491},
  {name:"SPA_SYSPROP_BARLTEDATA_ROAM", id:624},
  {name:"SPA_SYSPROP_INTLVOICE_ROAMGUARD", id:488},
  {name:"SPA_SYSPROP_DOMVOICE_ROAMGUARD_FORCED", id:480},
  {name:"SPA_SYSPROP_BARDOMVOICE_ROAM", id:481},
  {name:"SPA_SYSPROP_DOMVOICE_ROAM_FORCED", id:482},
  {name:"SPA_SYSPROP_INTLVOICE_ROAMGUARD_FORCED", id:489},
  {name:"SPA_SYSPROP_BARINTLVOICE_ROAM", id:490},
  {name:"SPA_SYSPROP_BARLTEDATA_ROAM_FORCED", id:625},
  {name:"SPA_SYSPROP_LTEDATA_ROAM_ENABLED", id:626},
  {name:"SPA_SYSPROP_LTEDATA_ROAMGUARD_FORCED", id:627},
  {name:"SPA_SYSPROP_BARINTL_LTEDATA_ROAM", id:628},
  {name:"SPA_SYSPROP_BARINTL_LTEDATA_ROAM_FORCED", id:629},
  {name:"SPA_SYSPROP_INTLLTEDATA_ROAMGUARD", id:630},
  {name:"SPA_SYSPROP_INTLLTEDATA_ROAMGUARD_FORCED", id:631}
];

function $(id) {
  return document.getElementById(id);
}

function onGetProperty(sysprop) {
  navigator.mozSysProp.getSysProp(sysprop.id, function(index, isSuccess, value, err) {
    if (err === "NoError") {
      dump("feong success" + sysprop.name + sysprop.id);
      str += "<p>[" + sysprop.name + "]: [" + value + "]</p>";
      // document.write("<p>[" + sysprop.name + "]: [" + value + "]</p>");
    } else {
      dump("feong failed" + sysprop.name + sysprop.id);
      str += "<p>[" + sysprop.name + "]: []</p>";
      // document.write("<p>[" + sysprop.name + "]: []</p>");
    }
    
    i++;
    if (i < sysprops.length) {
      onGetProperty(sysprops[i]);
    } else {
      $("textSprintExtensionProperties").innerHTML = str;
    }
  });
}

var i = 0;
var str = "";
onGetProperty(sysprops[i]);