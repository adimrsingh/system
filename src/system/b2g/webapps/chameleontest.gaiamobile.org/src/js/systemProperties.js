var sysprops = [
  "gsm.operator.alpha",
  "gsm.operator.idpstring",
  "gsm.operator.iso-country",
  "gsm.operator.isroaming",
  "gsm.operator.numeric",
  "gsm.operator.numeric1",
  "gsm.operator.numeric2",
  "gsm.sim.perator.alpha",
  "gsm.sim.operator.iso-country",
  "gsm.sim.operator.numeric",
  "gsm.sim.smsp",
  "gsm.sim.state",
  "ro.bootimage.build.fingerprint",
  "ro.build.fingerprint",
  "ro.cdma.home.operator.alpha",
  "ro.cdma.home.operator.numeric",
  "ro.com.google.clientidbase",
  "ro.com.google.clientidbase.am",
  "ro.com.google.clientidbase.gmm",
  "ro.com.google.clientidbase.ms",
  "ro.com.google.clientidbase.yt",
  "ro.device.wapprofile.url",
  "ro.home.operator.carrierid",
  "ro.setupwizard.launchtype",
  "ro_config_google_clientiddata1",
  "ro_config_google_clientiddata2",
  "ro_config_google_clientiddata3"
];

function $(id) {
  return document.getElementById(id);
}

function onGetProperty(sysprop) {
  return navigator.jrdExtension.readRoValue(sysprop);
}

var str = "";
for (var i = 0; i < sysprops.length; i++) {
  str += "<p>[" + sysprops[i] + "]: [" + onGetProperty(sysprops[i]) + "]</p>";
}
$("textSystemProperties").innerHTML = str;
