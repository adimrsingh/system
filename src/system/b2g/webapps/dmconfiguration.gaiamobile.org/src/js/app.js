'use strict';

function $(id) {
  return document.getElementById(id);
}

var SERVER_IDS =[{name:"Cingular", value:2},{name:"ATTLabA", value:3}];
var TYPE_VALUES =[{name:"OTAorWiFi", value:1},{name:"WiFi", value:2}];
var activeElement = "bootstrap-settings-li";
var elementsTable = [];
var SEC_ID ={HOME:0,BOOTSTRAP_SET:1,SERVER_SET:2,TRANSPORTTYPE_SET:3};

var account_info = new Array(15);
account_info[0]= $('appid-value');
account_info[1]= $('serverid-value');
account_info[2]= $('name-value');
account_info[3]= $('addr-value');
account_info[4]= $('portnbr-value');
account_info[5]= $('clientaauthlevel-value');
account_info[6]= $('clientaauthtype-value');
account_info[7]= $('clientaauthname-value');
account_info[8]= $('clientaauthsecret-value');
account_info[9]= $('clientaauthdata-value');
account_info[10]= $('serveraauthlevel-value');
account_info[11]= $('serveraauthtype-value');
account_info[12]= $('serveraauthname-value');
account_info[13]= $('serveraauthsecret-value');
account_info[14]= $('serveraauthdata-value');

var info_index =0;

var activeSec = SEC_ID.HOME;

var accountInfoIsLoading = false;

function onLoad() {
	dump("zhouling gaia : ---onLoad index.html");
	activeSec = SEC_ID.HOME;
	initMovements();
	activeElement = "bootstrap-settings-li";
	initFocus();
	initActions();
}

function onKeyDown(e) {
	dump("zhouling gaia : ---onKeyDown,key = "+e.key);
	var elActive = $(activeElement);
	switch(e.key) {
		case 'ArrowDown'://enter
			if(activeSec!=SEC_ID.BOOTSTRAP_SET){
				moveTo(elementsTable[elActive.id].down);
			}
			break;
		case 'ArrowUp'://enter
			if(activeSec!=SEC_ID.BOOTSTRAP_SET){
				moveTo(elementsTable[elActive.id].up);
			}
			break;
		case 'ArrowLeft'://left
			if(activeSec!=SEC_ID.BOOTSTRAP_SET){
				moveTo(elementsTable[elActive.id].left);
			}
			break;
		case 'ArrowRight'://right
			if(activeSec!=SEC_ID.BOOTSTRAP_SET){
				moveTo(elementsTable[elActive.id].right);
			}
			break;
		case 'Enter'://enter
			if(activeSec==SEC_ID.HOME){
				clickAction();
			}else if(activeSec==SEC_ID.BOOTSTRAP_SET){

			}else if(activeSec==SEC_ID.SERVER_SET){
				elActive.focus();
			}else if(activeSec==SEC_ID.TRANSPORTTYPE_SET){
				elActive.focus();
			}
			break;

		case 'Backspace'://backspace
			e.preventDefault();
			dump("zhouling gaia : ---index.html-Backspace");
			if(activeSec==SEC_ID.HOME){
				window.close();
			}else{
				if(!accountInfoIsLoading){
					switchPage(SEC_ID.HOME);
				}
			}
			break;
		default: //key not supported
			break;
	}
	
}

function clickAction() {
	dump("zhouling gaia : ---clickAction, activeElement = " +activeElement);
	switch(activeElement){
		case 'bootstrap-settings-li':
			switchPage(SEC_ID.BOOTSTRAP_SET);
			break;
		case 'dmserver-settings-li' :
			switchPage(SEC_ID.SERVER_SET);
			break;
		case 'transporttype-settings-li' :
			switchPage(SEC_ID.TRANSPORTTYPE_SET);
			break;
		default:
			break;
	}
}
	
function switchPage(sec_id) {
	$(activeElement).classList.remove('focus');
	$('sec-home').hidden=sec_id ==SEC_ID.HOME ?false:true;
	$('sec-bootstrap').hidden=sec_id ==SEC_ID.BOOTSTRAP_SET ?false:true;
	$('sec-dmserver').hidden=sec_id ==SEC_ID.SERVER_SET ?false:true;
	$('sec-transporttype').hidden=sec_id ==SEC_ID.TRANSPORTTYPE_SET ?false:true;
	activeSec=sec_id;
	init();
}

function onGetNode() {
	dump("zhouling gaia : ---onGetNode, info_index = "+info_index);	
	navigator.OmaService.getDMConfigList(info_index+1,getNodeBc);
}

function getNodeBc(cfgValue) {
	dump("zhouling gaia : ---getNodeBc, nodeValue = "+cfgValue);
	dump("zhouling gaia : ---getNodeBc, info_index = "+info_index);

	account_info[info_index].innerHTML=cfgValue;
	info_index++;
	if(info_index <15){
		onGetNode();
	}else{
		accountInfoIsLoading = false;
	}
}

window.addEventListener('load', onLoad);
window.addEventListener('keydown', onKeyDown);


function init() {
	dump("zhouling gaia : ---enter init");
	switch(activeSec){
		case SEC_ID.HOME:
			activeElement = "bootstrap-settings-li";
			initFocus();
			break;
		case SEC_ID.BOOTSTRAP_SET:
			info_index = 0;
			accountInfoIsLoading = true;
			onGetNode();
			break;
		case SEC_ID.SERVER_SET:
			activeElement = "buttonGetServer";
			initServerIdSelects();
			initFocus();
			break;
		case SEC_ID.TRANSPORTTYPE_SET:
			activeElement = "buttonGetType";
			initTypeValueSelects();
			initFocus();
			break;
		default:
			break;
	}
}

function initFocus() {
	// $(activeElement).focus();
	$(activeElement).classList.add('focus');
}

function initActions() {
	$("buttonSetServer").onclick=onSetServer;
	$("buttonGetServer").onclick=onGetServer;

	$("buttonSetType").onclick=onSetType;
	$("buttonGetType").onclick=onGetType;
}

function initServerIdSelects() {
	var selectE2 = $("selectServerId");
	selectE2.options.length=0;
	var serverIds = SERVER_IDS;
	for(var item in serverIds) {
		var option = document.createElement("option");
		option.text = serverIds[item].name;
		option.setAttribute("value", serverIds[item].value);
		selectE2.add(option);
	}
	selectE2.selectedIndex=0;
}

function initTypeValueSelects() {
	var selectE1 = $("selectTypeValue");
	selectE1.options.length=0;
	var typeValues = TYPE_VALUES;
	for(var item in typeValues) {
		var option = document.createElement("option");
		option.text = typeValues[item].name;
		option.setAttribute("value", typeValues[item].value);
		selectE1.add(option);
	}
	selectE1.selectedIndex=0;
}


function initMovements() {
	elementsTable = [];

	elementsTable["bootstrap-settings-li"] = {up:null, down:"dmserver-settings-li", left:null, right:null};
	elementsTable["dmserver-settings-li"] = {up:"bootstrap-settings-li", down:"transporttype-settings-li", left:null, right:null};
	elementsTable["transporttype-settings-li"] = {up:"dmserver-settings-li", down:null, left:null, right:null};

	elementsTable["selectServerId"] = {up:null, down:null, left:null, right:"buttonSetServer"};
	elementsTable["buttonSetServer"] = {up:null, down:"buttonGetServer", left:"selectServerId", right:null};
	elementsTable["buttonGetServer"] = {up:"buttonSetServer", down:null, left:null, right:null};

	elementsTable["selectTypeValue"] = {up:null, down:null, left:null, right:"buttonSetType"};
	elementsTable["buttonSetType"] = {up:null, down:"buttonGetType", left:"selectTypeValue", right:null};
	elementsTable["buttonGetType"] = {up:"buttonSetType", down:null, left:null, right:null};
}

function onGetServer() {
	navigator.OmaService.getDMConfigList(0,
		function(cfgValue) {
		    dump("zhouling gaia :  -----onGetServer="+cfgValue);
		    $("currentServerValue").value = cfgValue;
		}
	);
}

function onSetServer() {
	var nodeValue = $("selectServerId").selectedOptions[0].text;
	dump("zhouling gaia : ---onSetServer,onSetServer = "+nodeValue);
	navigator.OmaService.setDMNodeValue({
										"nodeIndex" : 0,
										"nodeVal"   : nodeValue
										},onSetNodeCb);
}

function onSetNodeCb(nodeIndex,result) {
	dump("zhouling gaia :  -----onSetNodeCb,nodeIndex = "+nodeIndex+",result = "+result);
}

function onGetType() {
	navigator.OmaService.getDMConfigList(17,
		function(cfgValue) {
		    dump("zhouling gaia :  -----onGetType="+cfgValue);
		    $("currentTypeValue").value = cfgValue;
		}
	);
}

function onSetType() {
	var nodeValue = $("selectTypeValue").selectedOptions[0].text;
	dump("zhouling gaia : ---onSetServer,onSetServer = "+nodeValue);
	navigator.OmaService.setDMNodeValue({
										"nodeIndex" : 17,
										"nodeVal"   : nodeValue
										},onSetNodeCb);
}

function moveTo(target) {
	if (target) {
		$(activeElement).className="";
		activeElement = target;
		// $(activeElement).className="active";
		// $(activeElement).focus();
		$(activeElement).classList.add('focus');
	}
}