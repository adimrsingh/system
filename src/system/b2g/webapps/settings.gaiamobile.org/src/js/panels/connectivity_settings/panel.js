define(['require','modules/settings_panel','shared/settings_listener','shared/airplane_mode_helper'],function(require) {
  

  var SettingsPanel = require('modules/settings_panel');
  var SettingsListener = require('shared/settings_listener');
  var AirplaneModeHelper = require('shared/airplane_mode_helper');

  return function ctor_connectivity_settings() {
    var elements = {};
    var airplaneStatus, airplaneEnabled;

    function _initSoftKey() {
      var params = {
        menuClassName: 'menu-button',
        header: {
          l10nId: 'message'
        },
        items: [{
          name: 'Select',
          l10nId: 'select',
          priority: 2,
          method: function() {}
        }]
      };

      SettingsSoftkey.init(params);
      SettingsSoftkey.show();
    }

    function _keyEventHandler(evt) {
      switch (evt.key) {
        case 'Enter':
          var select = document.querySelector('li.focus select');
          select && select.focus();
          break;
        default:
      }
    }

    function _updateItemState(airplaneEnabled) {
      elements.airplaneItem.value = airplaneEnabled;
      elements.networkContainer.setAttribute('aria-disabled',
        airplaneEnabled);
      elements.networkSmallItem.setAttribute('aria-disabled',
        airplaneEnabled);

      if (airplaneEnabled) {
        elements.networkHrefItem.removeAttribute('href');
      } else {
        elements.networkHrefItem.setAttribute('href', '#carrier');
      }
    }

    function _initUI() {
      airplaneStatus = AirplaneModeHelper.getStatus();
      airplaneEnabled = (airplaneStatus === 'enabled' || false);
      elements.airplaneItem.value = airplaneEnabled;

      _updateItemState(airplaneEnabled);
    }

    function _setAirplaneState(evt) {
      var enabled = (elements.airplaneItem.value === 'true' || false);
      AirplaneModeHelper.setEnabled(enabled);

      _updateItemState(enabled);
    }

    return SettingsPanel({
      onInit: function(panel) {
        elements = {
          WifiSmallItem: panel.querySelector('#wifi-desc'),
          airplaneItem: panel.querySelector('.airplaneMode-select'),
          networkSmallItem: panel.querySelector('#mobilenetworkanddata-desc'),
          networkContainer: panel.querySelector('#data-connectivity'),
          networkHrefItem: panel.querySelector('#menuItem-mobileNetworkAndData')
        };

        elements.airplaneItem.addEventListener('change', _setAirplaneState);

        window.addEventListener('panelready', e => {
          switch (e.detail.current) {
            case '#wifi':
            case '#carrier':
              // If other APP enter "Settings configure activity" page,
              // we should back to "connectivity-settings" instead of "root" page.
              if (NavigationMap._optionsShow === false) {
                var header = document.querySelectorAll('.current [data-href]');
                header[0].setAttribute('data-href',
                  '#connectivity-settings');
              }
              break;
            default:
          }
        });

        SettingsListener.observe('wifi.enabled', true, enabled => {
          var value = enabled ? 'on' : 'off';
          elements.WifiSmallItem.setAttribute('data-l10n-id', value);
        });

        SettingsListener.observe('ril.data.enabled', false, enabled => {
          var value = enabled ? 'on' : 'off';
          elements.networkSmallItem.setAttribute('data-l10n-id', value);
        });
      },

      onShow: function() {
        _initUI();
      },

      onBeforeShow: function() {
        _initSoftKey();
        window.addEventListener('keydown', _keyEventHandler);
      },

      onBeforeHide: function() {
        SettingsSoftkey.hide();
      }
    });
  };
});
