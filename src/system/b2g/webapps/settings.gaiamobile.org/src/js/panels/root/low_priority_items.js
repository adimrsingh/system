
/**
 * The moudle supports displaying bluetooth information on an element.
 *
 * @module panels/root/bluetooth_item
 */
define('panels/root/bluetooth_item',['require','modules/bluetooth/version_detector'],function(require) {
  

  var APIVersionDetector = require('modules/bluetooth/version_detector');

  var APIVersion = APIVersionDetector.getVersion();
  /**
   * @alias module:panels/root/bluetooth_item
   * @class BluetoothItem
   * @requires module:modules/bluetooth
   * @param {HTMLElement} element
                          The element displaying the bluetooth information
   * @return {BluetoothItem}
   */
  function BluetoothItem(element) {
    this._enabled = false;
    this._element = element;
    this._boundRefreshMenuDescription =
      this._refreshMenuDescription.bind(this, element);
  }

  BluetoothItem.prototype = {
    /**
     * Return Bluetooth API version via APIVersionDetector module.
     *
     * @access private
     * @memberOf BluetoothItem.prototype
     * @type {Number}
     */
    _APIVersion: function bt__APIVersion() {
      return APIVersion;
    },

    /**
     * An instance to maintain that we have created a promise to get Bluetooth
     * module.
     *
     * @access private
     * @memberOf BluetoothItem.prototype
     * @type {Promise}
     */
    _getBluetoothPromise: null,

    /**
     * A promise function to get Bluetooth module.
     *
     * @access private
     * @memberOf BluetoothItem.prototype
     * @type {Promise}
     */
    _getBluetooth: function bt__getBluetooth() {
      if (!this._getBluetoothPromise) {
        this._getBluetoothPromise = new Promise(function(resolve) {
          var bluetoothModulePath;
          if (this._APIVersion() === 1) {
            bluetoothModulePath = 'modules/bluetooth/bluetooth_v1';
          } else if (this._APIVersion() === 2) {
            bluetoothModulePath = 'modules/bluetooth/bluetooth_context';
          }

          require([bluetoothModulePath], resolve);
        }.bind(this));
      }
      return this._getBluetoothPromise;
    },

    /**
     * Refresh the text based on the Bluetooth module enabled/disabled,
     * paired devices information.
     *
     * @access private
     * @memberOf BluetoothItem.prototype
     * @param {HTMLElement} element
                            The element displaying the bluetooth information
     */
    _refreshMenuDescription: function bt__refreshMenuDescription(element) {
      if (!navigator.mozL10n) {
        return;
      }

      this._getBluetooth().then(function(bluetooth) {
        if (bluetooth.enabled) {
          element.setAttribute('data-l10n-id', 'on');
        } else {
          element.setAttribute('data-l10n-id', 'off');
        }
      });
    },

    /**
     * The value indicates whether the module is responding.
     *
     * @access public
     * @memberOf BluetoothItem.prototype
     * @type {Boolean}
     */
    get enabled() {
      return this._enabled;
    },

    set enabled(value) {
      if (this._enabled === value) {
        return;
      }

      this._enabled = value;
      this._getBluetooth().then(function(bluetooth) {
        if (this._enabled) {
          bluetooth.observe('enabled', this._boundRefreshMenuDescription);
          bluetooth.observe('numberOfPairedDevices',
            this._boundRefreshMenuDescription);
          this._boundRefreshMenuDescription();
        } else {
          bluetooth.unobserve('enabled', this._boundRefreshMenuDescription);
          bluetooth.unobserve('numberOfPairedDevices',
            this._boundRefreshMenuDescription);
        }
      }.bind(this));
    }
  };

  return function ctor_bluetoothItem(element) {
    return new BluetoothItem(element);
  };
});

/**
 * The moudle supports displaying language information on an element.
 *
 * @module panels/root/language_item
 */
define('panels/root/language_item',['require','shared/language_list'],function(require) {
  

  var LanguageList = require('shared/language_list');

  /**
   * @alias module:panels/root/language_item
   * @class LanguageItem
   * @param {HTMLElement} element
                          The element displaying the language information
   * @returns {LanguageItem}
   */
  function LanguageItem(element) {
    this._enabled = false;
    this._boundRefreshText = this._refreshText.bind(this, element);
  }

  LanguageItem.prototype = {
    /**
     * Refresh the text based on the language setting.
     *
     * @access private
     * @memberOf LanguageItem.prototype
     * @param {HTMLElement} element
                            The element displaying the language information
     */
    _refreshText: function l_refeshText(element) {
      // display the current locale in the main panel
      LanguageList.get(function displayLang(languages, currentLanguage) {
        element.textContent = LanguageList.wrapBidi(
          currentLanguage, languages[currentLanguage]);
      });
    },

    /**
     * The value indicates whether the module is responding.
     *
     * @access public
     * @memberOf LanguageItem.prototype
     * @type {Boolean}
     */
    get enabled() {
      return this._enabled;
    },

    set enabled(value) {
      if (this._enabled === value || !navigator.mozL10n) {
        return;
      }
      
      this._enabled = value;
      if (this._enabled) {
        window.addEventListener('localized', this._boundRefreshText);
        this._boundRefreshText();
      } else {
        window.removeEventListener('localized', this._boundRefreshText);
      }
    }
  };

  return function ctor_languageItem(element) {
    return new LanguageItem(element);
  };
});

/**
 * The moudle supports displaying battery information on an element.
 *
 * @module panels/root/battery_item
 */
define('panels/root/battery_item',['require','modules/battery'],function(require) {
  

  var Battery = require('modules/battery');

  /**
   * @alias module:panels/root/battery_item
   * @class BatteryItem
   * @requires module:modules/battery
   * @param {HTMLElement} element
                          The element displaying the battery information
   * @returns {BatteryItem}
   */
  function BatteryItem(element) {
    this._enabled = false;
    this._element = element;
    this._boundRefreshText = this._refreshText.bind(this, element);
  }

  BatteryItem.prototype = {
    /**
     * Refresh the text based on the Battery module.
     *
     * @access private
     * @memberOf BatteryItem.prototype
     * @param {HTMLElement} element
                            The element displaying the battery information
     */
    _refreshText: function b_refreshText(element) {
      if (!navigator.mozL10n) {
        return;
      }

      navigator.mozL10n.setAttributes(element,
        'batteryLevel-percent-' + Battery.state, { level: Battery.level });
      if (element.hidden) {
        element.hidden = false;
      }
    },

    /**
     * The value indicates whether the module is responding.
     *
     * @access public
     * @memberOf BatteryItem.prototype
     * @type {Boolean}
     */
    get enabled() {
      return this._enabled;
    },

    set enabled(value) {
      if (this._enabled === value) {
        return;
      }
      
      this._enabled = value;
      if (this._enabled) {
        Battery.observe('level', this._boundRefreshText);
        Battery.observe('state', this._boundRefreshText);
        this._boundRefreshText();
      } else {
        Battery.unobserve('level', this._boundRefreshText);
        Battery.unobserve('state', this._boundRefreshText);
      }
    }
  };

  return function ctor_batteryItem(element) {
    return new BatteryItem(element);
  };
});

/* global DeviceStorageHelper, openIncompatibleSettingsDialog */
/**
 * Links the root panel list item with USB Storage.
 *
 * XXX bug 973451 will remove media storage part
 */
define('panels/root/storage_usb_item',['require','shared/settings_listener','shared/async_storage','modules/settings_cache','modules/settings_service'],function(require) {
  

  var SettingsListener = require('shared/settings_listener');
  var AsyncStorage = require('shared/async_storage');
  var SettingsCache = require('modules/settings_cache');
  var SettingsService = require('modules/settings_service');

  const MIN_MEDIA_FREE_SPACE_SIZE = 10 * 1024 * 1024;

  /**
   * @alias module:panels/root/storage_usb_item
   * @class USBStorageItem
   * @param {Object} elements
                     elements displaying the usb and media storage information
   * @returns {USBStorageItem}
   */
  function USBStorageItem(elements) {
    this._enabled = false;
    this._elements = elements;
    this._umsSettingKey = 'ums.enabled';
    // XXX media related attributes
    this._defaultMediaVolume = null;
    this._defaultVolumeState = 'available';
    this._defaultMediaVolumeKey = 'device.storage.writable.name';
    this._boundUmsSettingHandler = this._umsSettingHandler.bind(this);
    this._boundMediaVolumeChangeHandler =
      this._mediaVolumeChangeHandler.bind(this);
      this.umsEnabled = false;
  }

  USBStorageItem.prototype = {
    /**
     * The value indicates whether the module is responding. If it is false, the
     * UI stops reflecting the updates from the root panel context.
     *
     * @access public
     * @memberOf USBStorageItem.prototype
     * @type {Boolean}
     */
    get enabled() {
      return this._enabled;
    },

    set enabled(value) {
      if (this._enabled === value) {
        return;
      } else {
        this._enabled = value;
      }
      if (value) { //observe
        var self = this ;
       SettingsCache.getSettings(function(result){
         self.umsEnabled = result[self._umsSettingKey];
         self._updateUmsDesc();

        });

        SettingsListener.observe(this._umsSettingKey, false,
          this._boundUmsSettingHandler);
        // media storage
        // Show default media volume state on root panel
        SettingsListener.observe(this._defaultMediaVolumeKey, 'sdcard',
          this._boundMediaVolumeChangeHandler);
        window.addEventListener('localized', this);

        // register USB storage split click handler
      } else { //unobserve


        SettingsListener.unobserve(this._umsSettingKey,
          this._boundUmsSettingHandler);
        // media storage
        SettingsListener.unobserve(this._defaultMediaVolumeKey,
          this._boundMediaVolumeChangeHandler);
        window.removeEventListener('localized', this);

      }
    },

    _umsSettingHandler: function storage_umsSettingHandler(enabled) {
      this.umsEnabled = enabled;
      this._updateUmsDesc();
    },
    handleEvent: function storage_handleEvent(evt) {
      switch (evt.type) {
        case 'localized':
          this._updateMediaStorageInfo();
          break;
        case 'change':
            // we are handling storage state changes
            // possible state: available, unavailable, shared
            this._updateMediaStorageInfo();

          break;
      }
    },

    // ums description
    _updateUmsDesc: function storage_updateUmsDesc() {
      var key;
      if (this.umsEnabled) {
        //TODO list all enabled volume name
        key = 'enabled';
      } else if (this._defaultVolumeState === 'shared') {
        key = 'umsUnplugToDisable';
      } else {
        key = 'disabled';
      }
      this._elements.usbEnabledInfoBlock.setAttribute('data-l10n-id', key);
    },

    _umsMasterSettingChanged: function storage_umsMasterSettingChanged(evt) {
      var checkbox = evt.target;
      var cset = {};
      var warningKey = 'ums-turn-on-warning';

      if (checkbox.checked) {
        AsyncStorage.getItem(warningKey, function(showed) {
          if (!showed) {
            this._elements.umsWarningDialog.hidden = false;

            this._elements.umsConfirmButton.onclick = function() {
              AsyncStorage.setItem(warningKey, true);
              this._elements.umsWarningDialog.hidden = true;

              SettingsCache.getSettings(
                this._openIncompatibleSettingsDialogIfNeeded.bind(this));
            }.bind(this);

            this._elements.umsCancelButton.onclick = function() {
              cset[this._umsSettingKey] = false;
              Settings.mozSettings.createLock().set(cset);

              checkbox.checked = false;
              this._elements.umsWarningDialog.hidden = true;
            }.bind(this);
          } else {
            SettingsCache.getSettings(
              this._openIncompatibleSettingsDialogIfNeeded.bind(this));
          }
        }.bind(this));
      } else {
        cset[this._umsSettingKey] = false;
        Settings.mozSettings.createLock().set(cset);
      }
    },

    _openIncompatibleSettingsDialogIfNeeded:
      function storage_openIncompatibleSettingsDialogIfNeeded(settings) {
        var cset = {};
        var umsSettingKey = this._umsSettingKey;
        var usbTetheringSetting = settings['tethering.usb.enabled'];

        if (!usbTetheringSetting) {
          cset[umsSettingKey] = true;
          Settings.mozSettings.createLock().set(cset);
        } else {
          var oldSetting = 'tethering.usb.enabled';
          openIncompatibleSettingsDialog('incompatible-settings-warning',
            umsSettingKey, oldSetting, null);
        }
    },

    // XXX media related functions
    _mediaVolumeChangeHandler:
      function storage_mediaVolumeChangeHandler(defaultName) {
      if (this._defaultMediaVolume) {
        this._defaultMediaVolume.removeEventListener('change', this);
      }
      this._defaultMediaVolume = this._getDefaultVolume(defaultName);
      this._defaultMediaVolume.addEventListener('change', this);
      this._updateMediaStorageInfo();
    },

    // Media Storage
    _updateMediaStorageInfo: function storage_updateMediaStorageInfo() {
      if (!this._defaultMediaVolume) {
        return;
      }

      var self = this;
      this._defaultMediaVolume.available().onsuccess = function(evt) {
        var state = evt.target.result;
        var firstVolume = navigator.getDeviceStorages('sdcard')[0];
        // if the default storage is unavailable, and it's not the
        // internal storage, we show the internal storage status instead.
        if (state === 'unavailable' &&
          self._defaultMediaVolume.storageName !== firstVolume.storageName) {
          firstVolume.available().onsuccess = function(e) {
            self._updateVolumeState(firstVolume, e.target.result);
          };
        } else {
          self._updateVolumeState(self._defaultMediaVolume, state);
        }
      };
    },

    _updateVolumeState: function storage_updateVolumeState(volume, state) {
      this._defaultVolumeState = state;
      this._updateUmsDesc();
      switch (state) {
        case 'available':
          this._updateMediaFreeSpace(volume);
          this._lockMediaStorageMenu(false);
          break;

        case 'shared':
          this._elements.mediaStorageDesc.removeAttribute('data-l10n-id');
          this._elements.mediaStorageDesc.textContent = '';
          this._lockMediaStorageMenu(false);
          break;

        case 'unavailable':
          this._elements.mediaStorageDesc.setAttribute('data-l10n-id',
                                                       'no-storage');
          this._lockMediaStorageMenu(true);
          break;
      }
    },

    _updateMediaFreeSpace: function storage_updateMediaFreeSpace(volume) {
      var self = this;
      volume.freeSpace().onsuccess = function(e) {
        DeviceStorageHelper.showFormatedSize(self._elements.mediaStorageDesc,
          'availableSize', e.target.result);
        if (e.target.result < MIN_MEDIA_FREE_SPACE_SIZE) {
          self._elements.mediaStorageDesc.parentNode.classList.add('alert');
        } else {
          self._elements.mediaStorageDesc.parentNode.classList.remove('alert');
        }
      };
    },

    _lockMediaStorageMenu: function storage_setMediaMenuState(lock) {
      if (lock) {
        this._elements.mediaStorageSection.setAttribute('aria-disabled', true);
        this._elements.mediaStorageSection.querySelector("a").removeAttribute("href");
      } else {
        this._elements.mediaStorageSection.removeAttribute('aria-disabled');
        this._elements.mediaStorageSection.querySelector('a').setAttribute('href',"#mediaStorage");
      }
    },

    // util function
    _getDefaultVolume: function storage_getDefaultVolume(name) {
      // Per API design, all media type return the same volumes.
      // So we use 'sdcard' here for no reason.
      // https://bugzilla.mozilla.org/show_bug.cgi?id=856782#c10
      var volumes = navigator.getDeviceStorages('sdcard');
      if (!name || name === '') {
        return volumes[0];
      }
      for (var i = 0; i < volumes.length; ++i) {
        if (volumes[i].storageName === name) {
          return volumes[i];
        }
      }
      return volumes[0];
    }
  };

  return function ctor_usb_storage_item(elements) {
    return new USBStorageItem(elements);
  };
});

/* global DeviceStorageHelper */
/**
 * Links the root panel list item with AppStorage.
 */
define('panels/root/storage_app_item',['require','modules/app_storage'],function(require) {
  

  var AppStorage = require('modules/app_storage');
  const MIN_APP_FREE_SPACE_SIZE = 20 * 1024 * 1024;

  /**
   * @alias module:panels/root/storage_app_item
   * @class AppStorageItem
   * @requires module:modules/app_storage
   * @param {HTMLElement} element
                          The element displaying the app storage information
   * @returns {AppStorageItem}
   */
  function AppStorageItem(element) {
    this._enabled = false;
    this._element = element;
    this._boundUpdateAppFreeSpace = this._updateAppFreeSpace.bind(this);
    this._boundUpdateLowSpaceDisplay = this._updateLowSpaceDisplay.bind(this);
  }

  AppStorageItem.prototype = {
    /**
     * The value indicates whether the module is responding. If it is false, the
     * UI stops reflecting the updates from the root panel context.
     *
     * @access public
     * @memberOf AppStorageItem.prototype
     * @type {Boolean}
     */
    get enabled() {
      return this._enabled;
    },

    set enabled(value) {
      if (this._enabled === value) {
        return;
      } else {
        this._enabled = value;
      }
      if (value) { //observe
        AppStorage.storage.observe('freeSize', this._boundUpdateAppFreeSpace);
        AppStorage.storage.observe('usedPercentage',
          this._boundUpdateLowSpaceDisplay);
        this._updateAppFreeSpace();
        window.addEventListener('localized', this);
      } else { //unobserve
        AppStorage.storage.unobserve('freeSize', this._boundUpdateAppFreeSpace);
        AppStorage.storage.unobserve('usedPercentage',
          this._boundUpdateLowSpaceDisplay);
        window.removeEventListener('localized', this);
      }
    },

    _updateLowSpaceDisplay: function storage__updateUsePercentage() {
      if (AppStorage.storage.usedPercentage >= 90 ||
        AppStorage.storage.freeSize < MIN_APP_FREE_SPACE_SIZE) {
        this._element.parentNode.classList.add('alert');
      } else {
        this._element.parentNode.classList.remove('alert');
      }
    },

    // Application Storage
    _updateAppFreeSpace: function storage_updateAppFreeSpace() {
      DeviceStorageHelper.showFormatedSize(this._element,
        'availableSize', AppStorage.storage.freeSize);
      this._updateLowSpaceDisplay();
    },

    handleEvent: function storage_handleEvent(evt) {
      switch (evt.type) {
        case 'localized':
          this._updateAppFreeSpace();
          break;
      }
    }
  };

  return function ctor_app_storage_item(element) {
    return new AppStorageItem(element);
  };
});

define('panels/root/wifi_item',['require','modules/wifi_context'],function(require) {
  

  var WifiContext = require('modules/wifi_context');
  var wifiManager = navigator.mozWifiManager;

  function WifiItem(element) {
    this._enabled = false;
    this._boundUpdateWifiDesc = this._updateWifiDesc.bind(this, element);
  }

  WifiItem.prototype = {
    set enabled(value) {
      if (value === this._enabled || !wifiManager) {
        return;
      }

      this._enabled = value;
      if (this._enabled) {
        this._boundUpdateWifiDesc();
        WifiContext.addEventListener('wifiStatusTextChange',
          this._boundUpdateWifiDesc);
      } else {
        WifiContext.removeEventListener('wifiStatusTextChange',
          this._boundUpdateWifiDesc);
      }
    },

    get enabled() {
      return this._enabled;
    },

    _updateWifiDesc: function root_updateWifiDesc(element) {
      if (WifiContext.wifiStatusText.id === 'disabled') {
        element.setAttribute('data-l10n-id', 'off');
      } else {
        element.setAttribute('data-l10n-id', 'on');
      }
    }
  };

  return function ctor_wifiItem(element) {
    return new WifiItem(element);
  };
});

// set the default status of mobile network & data
define('panels/root/mobile_network_and_data_item',['require'],function(require) {
  
  function MobileNetworkAndDataItem(element) {
    var _self = this;
    this._enabled = false;
    this.key = 'ril.data.enabled';
    var dataConnection = navigator.mozSettings.createLock().get(_self.key);
    dataConnection.onsuccess = () => {
      var enabled = dataConnection.result[_self.key];
      element.setAttribute('data-l10n-id', enabled ? 'on' : 'off');
    };
  }

  return function ctor_MobileNetworkAndDataItem(element) {
    return new MobileNetworkAndDataItem(element);
  };
});

/* global DeviceStorageHelper, openIncompatibleSettingsDialog */
define('panels/root/antitheft_item',['require','shared/settings_listener','modules/settings_cache'],function(require) {
  

  var SettingsListener = require('shared/settings_listener');
  var SettingsCache = require('modules/settings_cache');

  function AntitheftItem(elements) {
    this._enabled = false;
    this._elements = elements;
    this._antitheftSettingKey = 'antitheft.enabled';
    this._boundAntitheftSettingHandler = this._antitheftSettingHandler.bind(this);
    this.antitheftEnabled = false;

    LazyLoader.load([
          '/shared/js/fxa_iac_client.js'
        ], function fxa_panel_loaded() {
          FxAccountsIACHelper.getAccounts(onGetAccountsSuccess, onGetAccountsFailed);
        }
    );
  }

  function onGetAccountsSuccess(e) {
    var link = document.getElementById('menuItem-antitheft');
    var note1 = document.getElementById('menuItem-antitheft-note1');
    var note2 = document.getElementById('menuItem-antitheft-note2');

    if (!e) {
      link.classList.add('disabled');
      note1.classList.add('hidden');
      note2.classList.add('hidden');
      if (link.hasAttribute('href')) {
        link.removeAttribute('href');
      }
    } else if (e.verified) {
      link.classList.remove('disabled');
      note1.classList.remove('hidden');
      note2.classList.remove('hidden');
      if (!link.hasAttribute('href')) {
        link.setAttribute('href', '#antitheft');
      }
    }
  }

  function onGetAccountsFailed(e) {
    var link = document.getElementById('menuItem-antitheft');
    var note1 = document.getElementById('menuItem-antitheft-note1');
    var note2 = document.getElementById('menuItem-antitheft-note2');
    link.classList.remove('disabled');
    note1.classList.remove('hidden');
    note2.classList.remove('hidden');
    if (!link.hasAttribute('href')) {
      link.setAttribute('href', '#antitheft');
    }
  }

  AntitheftItem.prototype = {

    get enabled() {
      return this._enabled;
    },

    set enabled(value) {
      if (this._enabled === value) {
        return;
      } else {
        this._enabled = value;
      }
      if (value) {
        var self = this;
        SettingsCache.getSettings(function(result) {
          self.antitheftEnabled = result[self._antitheftSettingKey];
          self._updateAntitheftDesc();
        });

        SettingsListener.observe(this._antitheftSettingKey, false,
            this._boundAntitheftSettingHandler);

      } else {

        SettingsListener.unobserve(this._antitheftSettingKey,
            this._boundAntitheftSettingHandler);

      }
    },

    _antitheftSettingHandler: function storage_antitheftSettingHandler(enabled) {
      this.antitheftEnabled = enabled;
      this._updateAntitheftDesc();
    },

    _updateAntitheftDesc: function storage_updateAntitheftDesc() {
      var key;
      if (this.antitheftEnabled) {
        key = 'on';
      } else {
        key = 'off';
      }
      this._elements.setAttribute('data-l10n-id', key);
    }

  };

  return function ctor_antitheft_item(elements) {
    return new AntitheftItem(elements);
  };

});

/**
 * This module contains modules for the low priority items in the root panel.
 * The module should only be loaded after the menu items are ready for user
 * interaction.
 *
 * @module panels/root/low_priority_items
 */
define('panels/root/low_priority_items',['require','panels/root/bluetooth_item','panels/root/language_item','panels/root/battery_item','panels/root/storage_usb_item','panels/root/storage_app_item','panels/root/wifi_item','panels/root/mobile_network_and_data_item','panels/root/antitheft_item'],function(require) {
  

  var items = {
    BluetoothItem: require('panels/root/bluetooth_item'),
    LanguageItem: require('panels/root/language_item'),
    BatteryItem: require('panels/root/battery_item'),
    StorageUSBItem: require('panels/root/storage_usb_item'),
    StorageAppItem: require('panels/root/storage_app_item'),
    WifiItem: require('panels/root/wifi_item'),
    MobileNetworkAndDataItem: require('panels/root/mobile_network_and_data_item'),
    AntitheftItem: require('panels/root/antitheft_item')
  };

  return {
    get BluetoothItem()    { return items.BluetoothItem; },
    get LanguageItem()     { return items.LanguageItem; },
    get BatteryItem()      { return items.BatteryItem; },
    get StorageUSBItem()   { return items.StorageUSBItem; },
    get StorageAppItem()   { return items.StorageAppItem; },
    get WifiItem()         { return items.WifiItem; },
    get MobileNetworkAndDataItem() {return items.MobileNetworkAndDataItem; },
    get AntitheftItem()    { return items.AntitheftItem; }
  };
});
