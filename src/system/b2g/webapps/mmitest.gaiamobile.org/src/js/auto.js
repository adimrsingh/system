/* © 2017 KAI OS TECHNOLOGIES (HONG KONG) LIMITED All rights reserved.
 * This file or any portion thereof may not be reproduced or used in any manner
 * whatsoever without the express written permission of KAI OS TECHNOLOGIES
 * (HONG KONG) LIMITED. KaiOS is the trademark of KAI OS TECHNOLOGIES (HONG KONG)
 * LIMITED or its affiliate company and may be registered in some jurisdictions.
 * All other trademarks are the property of their respective owners.
 */
// ************************************************************************
// * File Name: auto.js
// * Description: mmitest -> auto test part.
// * Note: When you want to add a new test item, just add the html file
// *       name in array testList
// ************************************************************************

/* global dump, asyncStorage */
'use strict';

const RESULT_LIST = 'result_list';
const RESULT_FILE_NAME = 'auto_test_result.txt';

function debug(s) {
  if (DEBUG) {
    dump('<mmitest> ------: [auto.js] = ' + s + '\n');
  }
}

var AutoTest = {
  testList: [],
  resultList: [],
  index: 0,
  // 'start', 'next', the start button will be used in two cases.
  autoStartButtonStatus: 'start',

  get mainPanel () {
    return document.getElementById('main-panel');
  },

  get testPanel () {
    return document.getElementById('test-panel');
  },

  get resultPanel () {
    return document.getElementById('result-screen');
  },

  get autoStartButton() {
    return document.getElementById('autoStartButton');
  },

  get autoEndButton() {
    return document.getElementById('autoEndButton');
  },

  get retestButton() {
    return document.getElementById('retestButton');
  },

  get resultText() {
    return document.getElementById('result-text');
  },

  get centerContext() {
    return document.getElementById('centertext');
  },

  get iframe() {
    return document.getElementById('test-iframe');
  },

  get restartYes() {
    return document.getElementById('restartYes');
  },

  get restartNo() {
    return document.getElementById('restartNo');
  },

  startGps: function _startGps() {
    if (navigator.engmodeExtension) {
      navigator.engmodeExtension.startGpsTest();
    }
  },

  stopGps: function _stopGps() {
    if (navigator.engmodeExtension) {
      navigator.engmodeExtension.stopGpsTest();
    }
  },

  showPanel: function AutoTest_showpanel(panelName) {
    if (!panelName) {
      return
    }

    this.mainPanel.classList.add('hidden');
    this.testPanel.classList.add('hidden');
    this.resultPanel.classList.add('hidden');
    document.getElementById(panelName).classList.remove('hidden');
  },

  getResultList: function AutoTest_geResultList() {
    return localStorage.getItem(RESULT_LIST);
  },

  setResultList: function AutoTest_setResult() {
    let resultList = this.resultList.toString();
    localStorage.setItem(RESULT_LIST, resultList);
    debug('setResult, result list:' + resultList);
  },

  clearResultList: function _clearResultList() {
    localStorage.setItem(RESULT_LIST, '');
    debug('Clear the result list.');
  },

  createResultFile: function at_setTestResultFileData(autoTestResult) {
    this.sdcard.available().then((result) => {
      switch (result) {
        case 'available':
          let text = new Blob([autoTestResult], {
            type: 'text/plain'
          });
          // We should delete the previous file and create a new file.
          this.sdcard.delete(RESULT_FILE_NAME).then(() => {
            this.sdcard.addNamed(text, RESULT_FILE_NAME).then((result) => {
              debug('Success to save the auto test result file: ' + result);
            }, (error) => {
              debug('Unable to save the test result file error: ' + error.name);
            });
          });
          break;
        case 'unavailable':
          debug('Your device\'s SD Card is not available.');
          break;
        default:
          debug('Your device\'s SD Card is shared and thus not available.');
          break;
      }
    }).catch((error) => {
      debug('Unable to get the space used by the SD Card: ' + error.name);
    });
  },

  initStorageLocation: function at_initStorageLocation() {
    let sdcards = navigator.getDeviceStorages('sdcard');
    // We should save this file into device internal sdcard storage area.
    this.sdcard = sdcards[0];
  },

  restart: function AutoTest_restartAutoTest() {
    this.index = 0;
    this.resultList = [];
    this.clearResultList();
    this.start();
  },

  start: function AutoTest_startAutoTest() {
    // If auto test end, we should display the test result page.
    if (this.index >= this.testList.length) {
      this.autoStartButtonStatus = 'end';
      this.displayResultList();
      return;
    }

    this.showPanel('test-panel');
    this.autoStartButton.innerHTML = 'Start';
    this.autoStartButtonStatus = 'start';
    this.retestButton.style.visibility = 'hidden';

    this.iframe.src = this.testList[this.index].htmlId + '.html';
    this.iframe.focus();
  },

  end: function AutoTest_endAutoTest() {
    this.stopGps();
    this.stopNfc();
    this.setResultList();
    this.createResultFile(this.parseResultList());
    window.location = '../index.html';
  },

  goToNext: function AutoTest_goToNextTest() {
    this.index += 1;
    if (this.index < this.testList.length) {
      this.iframe.src = this.testList[this.index].htmlId + '.html';
    } else {
      this.setResultList();
      this.createResultFile(this.parseResultList());
      this.displayResultList();
      this.autoStartButtonStatus = 'end';
    }
  },

  onFailed: function AutoTest_failAutoTest() {
    let item = this.testList[this.index];
    debug('AutoTest ' + item.itemName + ' item test failed.');
    this.setResultList();

    this.autoStartButton.innerHTML = 'Next';
    this.autoStartButtonStatus = 'next';
    this.retestButton.style.visibility = 'visible';

    if (item) {
      this.centerContext.innerHTML = item.itemName +
        ' test failed <br > Press Next or Retest';
    }

    this.showPanel('main-panel');
    this.iframe.blur();
  },

  parseResultList: function at_parseRestultList(list) {
    if (list) {
      this.resultList = list.split(',');
    }

    debug('Show the auto test result: ' + this.resultList.toString());
    let resultContent = '',
      i = 0,
      len = this.testList.length;

    for (i; i < len; i++) {
      let resultItem = this.resultList[i];
      let result = '';
      if (resultItem && '' !== resultItem) {
        result = (resultItem === 'true' ? 'PASS' : 'FAIL');
      } else {
        result = 'NOT TEST'
      }

      resultContent += '<p class=' + resultItem + '>' +
        this.testList[i].htmlId + ': ' +
        result + '</p>';
    }
    return resultContent;
  },

  displayResultList: function AutoTest_displayResultList(resultList) {
    let resultContent = 'Auto test result is null.';
    if (resultList) {
      resultContent = this.parseResultList(resultList);
    } else {
      resultContent = this.parseResultList(this.getResultList());
    }
    this.showPanel('result-screen');
    this.iframe.blur();
    this.resultText.innerHTML = resultContent;
  },

  initAutoList: function ut_initAutoList() {
    getMenuList().then((list) => {
      let i = 0,
        len = list.length;

      for (i; i < len; i++) {
        let item = list[i];
        if (!item.autoTest.isHidden) {
          this.testList.push(item);
        }
      }
    }).then(() => {
      this.displayResultList(this.getResultList());
    });
  },

  stopNfc: function() {
    if (navigator.engmodeExtension) {
      navigator.engmodeExtension.execCmdLE(['nfc_stop'], 1);
    }
  },

  init: function AutoTest_init() {
    // start gps_test module when enter test.
    this.startGps();
    this.initAutoList();
    this.initStorageLocation();
    this.iframe.addEventListener('load', this);
    this.iframe.addEventListener('unload', this);
    this.retestButton.addEventListener('click', this);
    this.retestButton.style.visibility = 'hidden';
    this.restartYes.addEventListener('click', this);
    this.restartNo.addEventListener('click', this);
  },

  handleEvent: function AutoTest_handleEvent(evt) {
    switch (evt.type) {
      case 'click':
        if (evt.name === 'pass') {
          this.resultList[this.index] = 'true';
          if (this.testList[this.index].htmlId === 'audio') {
            this.iframe.blur();
            window.focus();
            var self = this;
            setTimeout(function() {
              self.index += 1;
              self.start();
            }, 500);
            return;
          }
          this.goToNext();
        } else if (evt.name === 'fail') {
          this.resultList[this.index] = false;
          this.onFailed();
        }
        break;
      case 'retestButton':
        if (this.retestButton.style.visibility !== 'hidden') {
          this.start();
        }
        break;
      default:
        break;
    }
  },

  handleKeydown: function AutoTest_handleKeydown(evt){
    evt.preventDefault();
    switch(evt.key){
      case 'SoftRight':
        this.end();
        break;

      case 'SoftLeft':
        if (!this.resultPanel.classList.contains('hidden') &&
            this.autoStartButtonStatus === 'end') {
          this.restart();
          return;
        }
        if (this.autoStartButtonStatus === 'next') {
          this.index += 1;
        }
        this.start();
        break;

      case 'ArrowUp':
        if (!this.resultPanel.classList.contains('hidden')) {
          this.resultText.scrollTop -= 60;
        } else {
          this.start();
        }
        break;

      case 'ArrowDown':
        if (!this.resultPanel.classList.contains('hidden')) {
          this.resultText.scrollTop += 60;
        }
        break;

      default:
        break;
    }
  }
};

window.onload = AutoTest.init.bind(AutoTest);
window.addEventListener('keydown', AutoTest.handleKeydown.bind(AutoTest));
