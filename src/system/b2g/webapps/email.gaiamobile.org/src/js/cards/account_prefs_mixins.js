define([ "require", "l10n!", "cards" ], function(e) {
    var t = e("l10n!"), n = e("cards");
    return {
        _bindPrefs: function(e, n, o, r, i, s) {
            if (e) {
                var a = this.nodeFromClass(e), c = this.account.syncInterval, d = String(c), u = [];
                "undefined" != typeof _secretDebug && _secretDebug.fastSync && (u = u.concat(_secretDebug.fastSync));
                var l = Array.slice(a.options, 0).some(function(e) {
                    return d === e.value;
                });
                l || -1 !== u.indexOf(c) || u.push(c), u.forEach(function(e) {
                    var n = document.createElement("option"), o = e / 1e3;
                    n.value = String(e), t.setAttributes(n, "settings-check-dynamic", {
                        n: o
                    }), a.appendChild(n);
                }), a.value = d, a.addEventListener("change", this.onChangeSyncInterval.bind(this), !1);
            }
            if (n) {
                var h = this.nodeFromClass(n);
                h.addEventListener("change", this.onNotifyEmailClick.bind(this), !1), h.value = this.account.notifyOnNew;
            }
            if (o) {
                var p = this.nodeFromClass(o);
                p.addEventListener("change", this.onSoundOnSendClick.bind(this), !1), p.value = this.account.playSoundOnSend;
            }
            if (r) {
                var f = this.nodeFromClass(r);
                f.addEventListener("change", this.onSignatureEnabledClick.bind(this), !1), f.value = this.identity.signatureEnabled;
                var m = this.nodeFromClass(i);
                this.identity.signatureEnabled ? (m.parentNode.style.display = "block", m.parentNode.classList.remove("hidden")) : (m.parentNode.style.display = "none", 
                m.parentNode.classList.add("hidden"));
            }
            i && (this.signatureBox = this.nodeFromClass(i), this.updateSignatureBox()), s && (this.accoutLabelNode = this.nodeFromClass(s), 
            this.updateAccountLabel());
        },
        nodeFromClass: function(e) {
            return this.getElementsByClassName(e)[0];
        },
        onChangeSyncInterval: function(e) {
            var t = parseInt(e.target.value, 10);
            console.log("sync interval changed to", t), this.account.modifyAccount({
                syncInterval: t
            });
        },
        onNotifyEmailClick: function(e) {
            var t = e.target.value;
            console.log("notifyOnNew changed to: " + t);
            var n;
            "true" === t ? n = !0 : "false" === t && (n = !1), this.account.modifyAccount({
                notifyOnNew: n
            });
        },
        onSoundOnSendClick: function(e) {
            var t = e.target.value;
            console.log("playSoundOnSend changed to: " + t);
            var n;
            "true" === t ? n = !0 : "false" === t && (n = !1), this.account.modifyAccount({
                playSoundOnSend: n
            });
        },
        onSignatureEnabledClick: function(e) {
            var t = e.target.value;
            console.log("signatureEnabled changed to: " + t);
            var n;
            "true" === t ? (n = !0, this.signatureBox.parentNode.style.display = "block", this.signatureBox.parentNode.classList.remove("hidden")) : "false" === t && (n = !1, 
            this.signatureBox.parentNode.style.display = "none", this.signatureBox.parentNode.classList.add("hidden")), 
            document.dispatchEvent(new Event("update-list")), this.identity.modifyIdentity({
                signatureEnabled: n
            });
        },
        updateSignatureBox: function() {
            var e = this.identity.signature || "", t = this.signatureBox, n = document.createTextNode(e), o = this.signatureBox.firstChild;
            o && "BR" !== o.tagName || (t.insertBefore(n, this.signatureBox.lastChild), t.innerHTML = t.innerHTML.replace(/\n/g, "<br>"));
        },
        updateAccountLabel: function() {
            var e = this.account.label || "", t = this.accoutLabelNode;
            t.value = e;
        },
        onClickSignature: function(e) {
            n.pushCard("settings_signature", "animate", {
                account: this.account,
                index: e
            }, "right");
        }
    };
});