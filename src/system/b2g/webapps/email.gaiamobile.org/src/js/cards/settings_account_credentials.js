
define(['require','l10n!','cards','./oauth2/fetch','./base','template!./settings_account_credentials.html'],function(require) {

var mozL10n = require('l10n!'),
    cards = require('cards'),
    oauthFetch = require('./oauth2/fetch');

var self;

return [
  require('./base')(require('template!./settings_account_credentials.html')),
  {
    onArgs: function(args) {
      this.account = args.account;
      this.headerLabel.textContent = this.account.name;

      // If we're not using password auth, then hide the password box.
      if (this.account.authMechanism !== 'password') {
        this.querySelector('.tng-account-server-password')
            .classList.add('collapsed');
      }

      this.usernameNodeSpan.innerHTML = this.account.username;
      this.passwordNodeInput.value = this.account.password;
      self = this;
    },

    handleKeyDown: function(e) {
      if (!document.getElementById('Symbols')) {
        switch (e.key) {
          case 'Backspace':
            e.preventDefault();
            if (!document.querySelector('#confirm-dialog-container' +
                                        ' gaia-confirm')) {
              self.onBack();
            }
            break;
          case 'Enter':
            e.preventDefault();
            e.stopPropagation();
            break;
        }
      }
    },

    onCardVisible: function(navDirection) {
      const CARD_NAME = this.localName;
      const QUERY_CHILD = '.tng-account-server-password';
      const CONTROL_ID = CARD_NAME + ' ' + QUERY_CHILD;

      var menuOptions = [
        {
          name: 'Cancel',
          l10nId: 'cancel',
          priority: 1,
          method: function() {
            self.onBack();
          }
        },
        {
          name: 'Save',
          l10nId: 'opt-save',
          priority: 3,
          method: function() {
            self.onClickSave();
          }
        }
      ];

      var reauthOptions = [
        {
          name: 'Cancel',
          l10nId: 'cancel',
          priority: 1,
          method: function() {
            self.onBack();
          }
        },
        {
          name: 'reauth',
          l10nId: 'settings-reauth',
          priority: 2,
          method: function() {
            self.onClickReauth();
          }
        }
      ];

      console.log(this.localName + '.onCardVisible, navDirection=' +
                  navDirection);
      NavigationMap.navSetup(CARD_NAME, QUERY_CHILD);
      NavigationMap.setCurrentControl(CONTROL_ID);
      NavigationMap.setFocus('first');
      if (this.account.authMechanism === 'oauth2') {
        menuOptions = reauthOptions;
      }
      NavigationMap.setSoftKeyBar(menuOptions);
      // as readout feature will make input element lost focus,
      // so delay 50ms to make sure input element can get focus.
      setTimeout( () => {
        var pwdNode = self.passwordNodeInput;
        var selEnd = pwdNode.value.length;
        pwdNode.focus();
        pwdNode.setSelectionRange(selEnd, selEnd);
      }, 50);
      this.addEventListener('keydown', self.handleKeyDown);
    },

    onBack: function() {
      cards.removeCardAndSuccessors(this, 'animate', 1);
    },

    onClickSave: function() {
      var password = this.passwordNodeInput.value;

      if (password) {
        this.account.modifyAccount({password: password});
        this.account.clearProblems();
        this.onBack();
        if (!window.navigator.onLine) {
          var mL10nId = 'noInternetConnection';
          Toaster.showToast({
            messageL10nId: mL10nId,
            latency: 2000
          });
        }
      } else {
        var dialogConfig = {
          title: {
            id: 'confirm-dialog-title',
            args: {}
          },
          body: {
            id: 'settings-password-empty',
            args: {}
          },
          accept: {
            l10nId:'dialog-button-ok',
            priority: 1,
            callback: function() {}
          }
        };

        var dialog = new ConfirmDialogHelper(dialogConfig);
        dialog.show(document.getElementById('confirm-dialog-container'));
      }
    },

    onClickReauth: function() {
      var oauth2 = this.account._wireRep.credentials.oauth2;
      oauthFetch(oauth2, {
        login_hint: this.account.username
      })
      .then(function(response) {
        if (response.status === 'success') {
          this.account.modifyAccount({ oauthTokens: response.tokens });

          // The user may have reauthed because they canceled an onbadlogin
          // card but came here to try to fix the problem, so ask to clear
          // problems if possible.
          this.account.clearProblems();

          // Successfully reauthed, nothing else to do on this card.
          this.onBack();
        }
      }.bind(this));
    },

    die: function() {
      this.removeEventListener('keydown', self.handleKeyDown);
    }
  }
];
});
