(function(e, t) {
    "object" == typeof exports && (define = function(e, t) {
        e = e.map.forEach(function(e) {
            return require(e);
        }), module.exports = t(e);
    }, define.amd = {}), "function" == typeof define && define.amd ? define([ "wbxml", "./codepages/Common", "./codepages/AirSync", "./codepages/Contacts", "./codepages/Email", "./codepages/Calendar", "./codepages/Move", "./codepages/ItemEstimate", "./codepages/FolderHierarchy", "./codepages/MeetingResponse", "./codepages/Tasks", "./codepages/ResolveRecipients", "./codepages/ValidateCert", "./codepages/Contacts2", "./codepages/Ping", "./codepages/Provision", "./codepages/Search", "./codepages/GAL", "./codepages/AirSyncBase", "./codepages/Settings", "./codepages/DocumentLibrary", "./codepages/ItemOperations", "./codepages/ComposeMail", "./codepages/Email2", "./codepages/Notes", "./codepages/RightsManagement" ], t) : e.ActiveSyncCodepages = t(WBXML, ASCPCommon, ASCPAirSync, ASCPContacts, ASCPEmail, ASCPCalendar, ASCPMove, ASCPItemEstimate, ASCPHierarchy, ASCPMeetingResponse, ASCPTasks, ASCPResolveRecipients, ASCPValidateCert, ASCPContacts2, ASCPPing, ASCPProvision, ASCPSearch, ASCPGAL, ASCPAirSyncBase, ASCPSettings, ASCPDocumentLibrary, ASCPItemOperations, ASCPComposeMail, ASCPEmail2, ASCPNotes, ASCPRightsManagement);
})(this, function(e, t, n, o, r, i, s, a, c, d, u, l, h, p, f, m, g, y, _, v, b, S, T, w, A, E) {
    var I = {
        Common: t,
        AirSync: n,
        Contacts: o,
        Email: r,
        Calendar: i,
        Move: s,
        ItemEstimate: a,
        FolderHierarchy: c,
        MeetingResponse: d,
        Tasks: u,
        ResolveRecipients: l,
        ValidateCert: h,
        Contacts2: p,
        Ping: f,
        Provision: m,
        Search: g,
        GAL: y,
        AirSyncBase: _,
        Settings: v,
        DocumentLibrary: b,
        ItemOperations: S,
        ComposeMail: T,
        Email2: w,
        Notes: A,
        RightsManagement: E
    };
    return e.CompileCodepages(I), I;
});